<?php
namespace app\components;
use yii\bootstrap\Collapse;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
class Tree{
	public $subTree;
	public $nodeName;
	private $nodeView;
	private $nodeCount;
	public function showTreeAsCollapse($path){
		if ($this->isLeaf()) return $this->showLeaf($path);
		$label=$this->nodeView.' '.$this->nodeCount;

		foreach ($this->subTree as $subTree) 
			$content[]=
				$subTree->showTreeAsCollapse(($path=='' ? $this->nodeName : $path.'/'.$this->nodeName));
			
				
		$widget=Collapse::widget([
			'items' => [
				[
					'label' =>$label,
					'content' =>$content,
					'contentOptions' => ['class' => 'in']
				],
			],
		]);
		return $widget;
	}
	public function showChildrens(){
		print_r($this->subTree);
	}
	public function haveNode ($nodeName){
		if (isset($this->subTree[$nodeName])) return true;
		else return false;
	}
	public function isLeaf(){
		return !is_array($this->subTree);
	}
	public function showLeaf($path){
		return 
			Html::a($this->nodeView.' '.$this->nodeCount, [
				Url::to('/shop/catalog'),
				'id' =>($path=='' ? $this->nodeName : $path.'/'.$this->nodeName)
			]);
	}
	public function addNode($node){
		if ($node!='') $this->subTree[$node->nodeName]=$node;
//		return $node;
	}
	public function nodeByName($name){
		return $this->subTree[$name];
	}
	public function __construct($nodeName, $nodeView, $nodeCount){
		if ($nodeName!='') $this->nodeName=$nodeName;
		//else throw new NotFoundHttpException('Не могу создать безымянную ноду');
		$this->nodeView=$nodeView;
		$this->nodeCount=$nodeCount;
		
	}
}
?>
