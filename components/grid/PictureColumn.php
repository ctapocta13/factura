<?php
namespace app\components\grid;
 
use yii\grid\DataColumn;
use yii\helpers\Html;
 
class PictureColumn extends DataColumn
{
	public $path;
	protected function renderDataCellContent($model, $key, $index)
	{
		$value = $this->getDataCellValue($model, $key, $index);
		$html='<p>'.$value.'</p>';
		$html.= Html::img($this->path.$value, ['class'=>'img-responsive']);

		return ($value === null)||$value=='' ? $this->grid->emptyCell : $html;
	}
 
}
?>
