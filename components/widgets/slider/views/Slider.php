<?php

use yii\helpers\Html;

use yii\bootstrap\Carousel;
	$list=[];
	$pictures=$model->getPhotos()
		->where(['status'=> 1])
		->andWhere(['<=', 'startActivity', time()])
		->andWhere(['>=', 'stopActivity', time()])
		->orderBy(['sortOrder'=>SORT_ASC])
		->all();
	foreach ($pictures as $picture){
		$list[]=
		(($picture['link']=='#') || ($picture['link']=='') ? '' : '<a href="http://'.$picture['link'].'">').
		Html::img('@web/images/slider/'.$picture['photoPath'], ['class'=>'img-responsive']).
		(($picture['link']=='#' ) || ($picture['link']=='') ? '' : '</a>');
	}
	if (sizeof($pictures)>0){
		echo Carousel::widget([
			'items' => $list,
			'options' => ['class'=>'slide'],
		]);
	}
?>
