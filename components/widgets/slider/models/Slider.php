<?php

namespace app\components\widgets\slider\models;
use yii\helpers\Url;
use Yii;

/**
 * This is the model class for table "slider".
 *
 * @property integer $id
 * @property string $photoPath
 * @property string $link
 * @property integer $status
 * @property integer $order
 * @property integer $startActivity
 * @property integer $stopActivity
 */
class Slider extends \yii\db\ActiveRecord
{
	const SLIDER_DIRECTORY='@web/images/slider/';
	public $newPicture;
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'slider';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['sliderId', 'photoPath'], 'required'],
			[['status', 'sortOrder'], 'integer'],
			[['photoPath', 'link'], 'string', 'max' => 255],
			[['startActivity', 'stopActivity'], 'date'/*,'format'=>'php:U'*/],
			['status', 'default', 'value' => 1],
			['sliderId' , 'integer'],
			['stopActivity', 'default', 'value' => '1924894800'],
			['startActivity', 'default', 'value' => '0'],
			['sortOrder', 'default', 'value' => '0'],
			['link', 'default', 'value' => '#'],
			[['newPicture'], 'file', 'skipOnEmpty' => true, 'extensions' => ['jpg', 'png', 'jpeg']]
		];
	}
	public function afterValidate ()
	{
		$this->startActivity= strtotime($this->startActivity);
		$this->stopActivity= strtotime($this->stopActivity);
		
//		return true;
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
//            'id' => 'ID',
			'photoPath' => 'Фотография',
			'link' => 'Ссылка',
			'sliderId' => 'Слайдер',
			'status' => 'Показывать',
			'sortOrder' => 'Индекс',
			'startActivity' => 'Начало показа',
			'stopActivity' => 'Конец показа',
			'newPicture' => 'Новое изображение',
		];
	}
	public function saveNewPicture($file)
	{
		if ($this->photoPath==$file->baseName.'.'.$file->extension) return;
		$newFileName=$file->baseName.'.'.$file->extension;
		$filename = $_SERVER['DOCUMENT_ROOT'].Url::to(self::SLIDER_DIRECTORY.$newFileName);
		if (file_exists($filename)) {
			$newFileName=md5(time()).'.'.$file->extension;
			$filename = $_SERVER['DOCUMENT_ROOT'].Url::to(self::SLIDER_DIRECTORY.$newFileName);
		}
		else {
			$this->deleteOldPicture();
		}
		$file->saveAs($filename);
		$this->photoPath=$newFileName;
	}
	public function deleteOldPicture()
	{
		if ($this->photoPath=='') return;
		$filename = $_SERVER['DOCUMENT_ROOT'].Url::to(self::SLIDER_DIRECTORY.$this->photoPath);
		if (file_exists($filename))
			if (!unlink($filename)) \Yii::$app->session->setFlash('error', 'Не могу удалить связанный файл!');
	}

	public function getParent()
	{
		return $this->hasOne(SliderName::className(), ['id' => 'sliderId']);
	}
	public function getParentName()
	{
		$parent = $this->parent;
	 
		return $parent ? $parent->name : '';
	}
}
