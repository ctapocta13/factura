<?php

namespace app\components\widgets\slider\models;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * This is the model class for table "sliderName".
 *
 * @property integer $id
 * @property string $name
 */
class SliderName extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'sliderName';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['name'], 'required'],
			[['name'], 'string', 'max' => 255],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'name' => 'Название',
		];
	}
	public function getList(){
		$records=SliderName::find()->all();
		return array_combine(
			ArrayHelper::getColumn($records,'id'),
			ArrayHelper::getColumn($records,'name'));
	}
	public function getPhotos()
	{
		return $this->hasMany(Slider::className(), ['sliderId'=>'id']);
	}
}
