<?php

namespace app\components\widgets\slider;

use Yii;
use app\components\widgets\slider\models\SliderName;


class SliderWidget extends \yii\bootstrap\Widget {
	public $id;
	public function init() {
		
		parent::init();
		if (!is_numeric($this->id)) $this->id=0;
	}
	public function run() {
		$model=SliderName::find($this->id)->one();
		return $this->render( 'Slider', ['model' => $model]);
	}
}
