<?php

namespace app\components\widgets\pages;
use app\components\widgets\pages\models\Pages;
//use app\components\Basket;
//use Yii;
//use app\models\News;

class PageWidget extends \yii\bootstrap\Widget {
	public $id;
	
	public function init() {
		parent::init();
		if (!is_numeric($this->id)) $this->id=1;
	}
	public function run() {
		$body=Pages::find()->where(['id' => $this->id])->one();
/*		$filename="pages/".$page['filename'];
		if (file_exists($filename)) $div=file_get_contents($filename);*/
		return '<div class="page-widget-'.$this->id.'">'.($body['body']=='' ? '' : $body['body']).'</div>';
	}
}
