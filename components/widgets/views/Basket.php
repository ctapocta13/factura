<?php

use yii\helpers\Html;
use yii\helpers\Url;
$this->registerJsFile(
	'@web/scripts/basket.js',
	['depends'=>'app\assets\AppAsset']
);
$state=$model->getState();
?>
<div class="widgets-basket" data-count="<?=$state['count']?>" data-total="<?=$state['sum']?>">
		<?=Html::a('<span class="glyphicon glyphicon-shopping-cart"></span><div class="basket-count text-center">'
			.$state['count'].'</div><span class="basket-sum">'.$state['sum'].'</span><span> руб.</span>',
			[Url::to('/shop/checkout')])?>
</div>
