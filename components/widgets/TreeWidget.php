<?php

namespace app\components\widgets;

use app\components\Tree;
use app\modules\shop\models\Products;
use app\modules\shop\models\Categories;

use Yii;


class TreeWidget extends \yii\bootstrap\Widget {
	
	public function init() {
		parent::init();
		//if (!is_numeric($this->countToShow)) $this->countToShow=3;
	}
	public function run() {
		$tree=(new \yii\db\Query())
		->select(['productsCat0','productsCat1','productsCat2','productsCat3', 'count(*) as counts'])
		->from(Products::tableName())
		->where(['productsIsActive'=>1])
		->andWhere(['>', 'productsPrice', 0])
		->andWhere(['>', 'productsStock', 0])
		->groupBy(['productsCat0','productsCat1','productsCat2','productsCat3'])
		->all()
		;
	$treeRoot= new Tree('','Каталог', Products::find()->toShow()->count());

	if (is_array($tree))
		foreach ($tree as $treeRow){//Каждая строка дерева
			$currentNode=$treeRoot;
			for ($i=0;$i<4;$i++){
				if (!$currentNode->haveNode($treeRow['productsCat'.$i]) && $treeRow['productsCat'.$i]!=''){
					$nodeView=Categories::findOne($treeRow['productsCat'.$i]);
					$treeTmp=new Tree ($treeRow['productsCat'.$i], $nodeView['categoriesShortName'], $treeRow['counts']);
					
					$currentNode->addNode($treeTmp);
				}
				else 
					if ($treeRow['productsCat'.$i]=='') continue;
				$currentNode=$currentNode->nodeByName($treeRow['productsCat'.$i]);
			}
		}
		return $this->render( 'Tree', ['model' => $treeRoot]);
	}
}
