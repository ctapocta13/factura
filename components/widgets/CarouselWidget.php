<?php

namespace app\components\widgets;
//use app\models\News;
use yii\helpers\Html;
use yii\bootstrap\Carousel;

class CarouselWidget extends \yii\bootstrap\Widget {
	public $pictures;
	public function init() {
		parent::init();
/*		if ($this->pictures===null ) {
			print_r ($this);
			//$this->countToShow=3;
		}*/
	}
	public function run() {
		$list='';
		foreach ($this->pictures as $picture){
			$list[]=Html::img($picture['path'], ['class'=>'img-responsive'/*$picture['class']*/]);
		}
		return Carousel::widget([
			'items' => $list,
			'options' => ['class'=>'slide'],
		]);
		//изображения 1288*725
	}
}
