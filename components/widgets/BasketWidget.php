<?php

namespace app\components\widgets;
use app\components\Basket;
use Yii;
//use app\models\News;

class BasketWidget extends \yii\bootstrap\Widget {
	
	public function init() {
		parent::init();
		//if (!is_numeric($this->countToShow)) $this->countToShow=3;
	}
	public function run() {
		$basket= new Basket;
		
		return $this->render( 'Basket', ['model' => $basket]);
	}
}
