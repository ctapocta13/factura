<?php
namespace app\components;
use app\modules\shop\models\Products;
use yii\helpers\ArrayHelper;
use Yii;
class Basket{
	public $products;
	public $name='Ваша корзина';
	public $basketId;

	public function getState(){
		$totalCount=$totalSum=0;
		if ($this->products!==false){
			foreach($this->products as $product){
				$totalCount+=$product['quant'];
				$totalSum+=$product['price']*$product['quant'];
			}
		}
		return [
				'sum'=>$totalSum,
				'count'=>$totalCount,
			];
	}
	public function inBasket($id){
		if ($this->products===false) return false;
		$keys=ArrayHelper::getColumn($this->products, 'id');
		$key=array_search($id, $keys);
		return $key;
	}
	public function substractFromBasket($id){
		$productId=$this->inBasket($id);
		if($productId===false) return false;
		else 
			if (--$this->products[$productId]['quant']==0) {
				$quant=false;
				unset ($this->products[$productId]);
			}
			else $quant=$this->products[$productId]['quant'];
		$this->saveBasket();
		return $quant;
	}
	public function addIntoBasket($id, $price=false){
		$product=Products::findOne($id);
		$price=$product->getPrice();
		$stock=$product->productsStock;
		if ($price>0 && $stock>0){
			$key=$this->inBasket($id);
			if ($key===false) {
				$this->products[]=['id'=>$id, 'price'=>$price, 'quant'=>1];
				$quant=1;
			}
			else {
				if (++$this->products[$key]['quant'] > $stock) $this->products[$key]['quant']=$stock;
				$quant=$this->products[$key]['quant'];
			}
			$this->saveBasket();
			return $quant;
		}
		else \Yii::$app->session->setFlash('error', 'Ошибка добавления товара в корзину');
		return false;
	}
	public function saveBasket(){
		Yii::$app->response->cookies->add((new \yii\web\Cookie(
			[
				'name' => $this->basketId, 
				'value' => $this->products,
				'expire'=>time() + 86400 * 365
			])));
	}
	public function clearBasket(){
		Yii::$app->response->cookies->remove($this->basketId);
	}
	public function dirtBasket(){
		$this->products[]=['id'=>'owl_home_wear_217741ekru_44', 'price'=>800, 'quant'=>1];
		$this->products[]=['id'=>'owl_home_wear_217761yellow_46', 'price'=>700, 'quant'=>12];
		$this->products[]=['id'=>'mouse_col_home_wear_211161cyan_dry_rose_42', 'price'=>7000, 'quant'=>1];
		$this->products[]=['id'=>'mouse_col_home_wear_211161cyan_dry_rose_44', 'price'=>500, 'quant'=>2];
		$this->products[]=['id'=>'mouse_col_home_wear_211161cyan_dry_rose_46', 'price'=>300, 'quant'=>3];
		//print_r($this->products);
		$this->saveBasket();
	}
	public function syncPrice(){
		if ($this->products===false) return true;
		
		foreach ($this->products as $key=>$basketElement){
			$product=Products::findOne($basketElement['id']);
			if ($this->products[$key]['quant']>$product->productsStock){
				\Yii::$app->session->setFlash('info', 'В связи с изменением остатков количество товара в корзине было уменьшено');
				if ($product->productsStock>0) $this->products[$key]['quant']=$product->productsStock;
				else {
					unset($this->products[$key]);
					continue;
				}
			}
			$this->products[$key]['price']=$product->getPrice();
		}
		$this->saveBasket();
	}
	public function __construct($cookieName='Basket'){
		$this->products=Yii::$app->request->cookies->getValue($cookieName, []);
		$this->basketId=$cookieName;
	}
}
