factura internet-shop project
============================
Доделать
-------------------
* slider/index
* CRUD для Products допилить фото
* дерево должно загружаться из файла
* файл дерева формируется при загрузке файла
* Поиск по некоторым полям с помощью orWhere like
* Костыль для квадратных метров
* routes для всего
* Личный кабинет - история заказов
* Отчеты - история заказов
* Экспорт в PDF ?
* Умный фильтр?
* Проверить валидацию непосредственно внутри модели на реальных данных - как быстрее
* придумать валидатор для телефона
* попробовать наследовать User в модуле shop и пользоваться им :-/


Проект интернет-магазина на [Yii 2](http://Домашнийдекор.рф)

DIRECTORY STRUCTURE
-------------------

      assets/             contains assets definition
      commands/           contains console commands (controllers)
      config/             contains application configurations
      controllers/        contains Web controller classes
      mail/               contains view files for e-mails
      models/             contains model classes
      runtime/            contains files generated during runtime
      tests/              contains various tests for the basic application
      vendor/             contains dependent 3rd-party packages
      views/              contains view files for the Web application
      web/                contains the entry script and Web resources



REQUIREMENTS
------------

* PHP 5.4.0.
* Yii2 framework
* MySQL

### Install via Composer
If you do not have [Composer](http://getcomposer.org/), you may install it by following the instructions
at [getcomposer.org](http://getcomposer.org/doc/00-intro.md#installation-nix).

You can then install this project template using the following command:

~~~
php composer.phar global require "fxp/composer-asset-plugin:~1.0.0"
php composer.phar create-project --prefer-dist --stability=dev yiisoft/yii2-app-basic basic
~~~

Now you should be able to access the application through the following URL, assuming `basic` is the directory
directly under the Web root.

~~~
http://localhost/basic/web/
~~~

CONFIGURATION
-------------

### Database

Edit the file `config/db.php` with real data, for example:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=factura',
    'username' => 'root',
    'password' => '1234',
    'charset' => 'utf8',
];
```

**NOTE:** Yii won't create the database for you, this has to be done manually before you can access it.

Also check and edit the other files in the `config/` directory to customize your application.
