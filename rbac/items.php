<?php
return [
    'permUserEdit' => [
        'type' => 2,
        'description' => 'Редактирование пользователей',
    ],
    'permContentEdit' => [
        'type' => 2,
        'description' => 'Редактирование контента',
    ],
    'user' => [
        'type' => 1,
        'description' => 'Покупатель',
    ],
    'admin' => [
        'type' => 1,
        'description' => 'Admin',
        'children' => [
            'editor',
            'permUserEdit',
            'user',
        ],
    ],
    'editor' => [
        'type' => 1,
        'description' => 'Редактор',
        'children' => [
            'permContentEdit',
        ],
    ],
];
