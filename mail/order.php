<?php
use yii\helpers\Html;
 
/* @var $this yii\web\View */
/* @var $user app\modules\user\models\User */
 
?>
<h2>Здравствуйте!</h2>
<p>Поступил заказ №<?=$order->id?> от <?=date('d.m.Y', $order->date)?></p>
<table>
	<tr><th>Имя контакта:</th><td><?=$order->customerName?></td></tr>
	<tr><th>Адрес:</th><td><?=$order->deliveryAddress?></td></tr>
	<tr><th>Телефон:</th><td><a href="tel:<?=$order->customerPhone?>"><?=$order->customerPhone?></a></td></tr>
	<?php if ($order->email):?>
		<tr><th>Email:</th><td><a href="mailto:<?=$order->email?>"><?=$order->email?></a></td></tr>
	<?php endif?>
	<?php if ($order->deliveryDate):?>
		<tr><th>Дата доставки:</th><td><?=date('d.m.Y', $order->deliveryDate)?></td></tr>
	<?php endif?>
</table>
<?php if ($order->comment):?>
		<strong>Коментарий к заказу:</strong><p><?=$order->comment?></p>
	<?php endif?>
<h3>Детали заказа</h3>
<table>
	<tr>
		<th>#</th><th>Наименование</th><th>slug</th><th>Количество</th><th>Сумма</th>
	</tr>
	<?php if (is_array($order->details)):?>
		<?php $i=1;$totalCount=$totalSum=0?>
		<?php foreach($order->details as $row):?>
			<tr>
				<td><?=$i++?></td><td><?=$row->product->getShortName()?></td><td><?=$row->productSlug?></td><td><?=$row->quant?></td><td><?=$row->quant*$row->price?></td>
				<?php $totalCount+=$row->quant;$totalSum+=$row->quant*$row->price?>
			</tr>
		<?php endforeach?>
	<tr><th></th><th>Итого</th><th></th><th><?=$totalCount?></th><th><?=$totalSum?></th></tr>
	</table>
	<?php else:?>
		</table>
		<p>Позиций нет :(</p>
	<?php endif?>
