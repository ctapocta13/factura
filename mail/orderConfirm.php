<?php
use yii\helpers\Html;
use Yii;

/* @var $this yii\web\View */
/* @var $user app\modules\user\models\User */
 
?>
<h2>Здравствуйте, <?=$order->customerName?>!</h2>
<p>Вы оформили заказ №<?=$order->id?> от <?=date('d.m.Y', $order->date)?> в интернет-магазине <?=Yii::$app->name?></p>
<p><strong>Дата доставки: <?=date('d.m.Y', $order->deliveryDate)?></strong></p>
<p><strong>Адрес доставки: <?=$order->deliveryAddress?></strong></p>
<p><strong>Коментарий к заказу: </strong></p><p><?=$order->comment?></p>

<h3>Детали заказа</h3>
<table>
	<tr>
		<th>№</th><th>Наименование</th><th>Количество</th><th>Сумма</th>
	</tr>
	<?php if (is_array($order->details)):?>
		<?php $i=1;$totalCount=$totalSum=0?>
		<?php foreach($order->details as $row):?>
			<tr>
				<td><?=$i++?></td><td><?=$row->product->getShortName()?></td><td><?=$row->quant?></td><td><?=$row->quant*$row->price?></td>
				<?php $totalCount+=$row->quant;$totalSum+=$row->quant*$row->price?>
			</tr>
		<?php endforeach?>
	<tr><th></th><th>Итого</th><th><?=$totalCount?></th><th><?=$totalSum?></th></tr>
	</table>
	<?php else:?>
		</table>
		<p>ПУсто</p>
	<?php endif?>
