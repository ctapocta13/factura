<?php
use yii\helpers\ArrayHelper;
$params = ArrayHelper::merge(
	require(__DIR__ . '/params.php'),
	require(__DIR__ . '/params-local.php')
);

$config = [
	'id' => 'factura',
	'name' => 'Домашнийдекор.рф',
	'basePath' => dirname(__DIR__),
	'bootstrap' => ['log'],
	'language' => 'ru-RU',
	'charset' => 'UTF-8',
	'sourceLanguage' => 'ru-RU',
	'timeZone' => 'Europe/Moscow',
	'modules' => [
		'user' => [
			'class' => 'app\modules\user\Module',
			'passwordResetTokenExpire' => 3600,
		],
		'shop' => [
			'class' => 'app\modules\shop\Module',
		],
		'admin' => [
			'class' => 'app\modules\admin\Module',
		],
	],
	'components' => [
		'request' => [
			// !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
			'cookieValidationKey' => 'fUqJ7w91fr8oSH6rofacturazcjMDWwvmC5TeJU',
			'enableCookieValidation' => true,
		],
		'cache' => [
			'class' => 'yii\caching\FileCache',
		],
		'user' => [
			'identityClass' => 'app\modules\user\models\User',
			'enableAutoLogin' => true,
			'loginUrl' => ['user/default/login'],
		],
		'authManager' => [
			'class' => 'app\components\AuthManager',
		],
		'errorHandler' => [
			'errorAction' => 'site/error',
		],
		'mailer' => [
			'class' => 'yii\swiftmailer\Mailer',
			// send all mails to a file by default. You have to set
			// 'useFileTransport' to false and configure a transport
			// for the mailer to send real emails.
			'useFileTransport' => false,
			'transport' => [
				'class' => 'Swift_SmtpTransport',
				'host' => 'smtp.mail.ru',  // e.g. smtp.mandrillapp.com or smtp.gmail.com
				'username' => 'ctapocta_13@mail.ru',
				'password' => '13135',
				'port' => '465', // Port 25 is a very common port too
				'encryption' => 'ssl',
			],
		],
		'log' => [
			'traceLevel' => YII_DEBUG ? 3 : 0,
			'targets' => [
				[
					'class' => 'yii\log\FileTarget',
					'levels' => ['error', 'warning'],
				],
			],
		],
		'db' => require(__DIR__ . '/db.php'),
		'urlManager' => [
			'enablePrettyUrl' => true,
//			'showScriptName' => false,
			'rules' => [
				'<_c:[\w\-]+>/<id:\d+>' => '<_c>/view',
				'<_c:[\w\-]+>' => '<_c>/index',
				'<_c:[\w\-]+>/<_a:[\w\-]+>/<id:\d+>' => '<_c>/<_a>',
				'<_c:[\w\-]+>/test<path:[/\w+]+>' =>'<_c>/test' ,
			],
		],
	],
	'params' => $params,
];

if (YII_ENV_DEV) {
	// configuration adjustments for 'dev' environment
	$config['bootstrap'][] = 'debug';
	$config['modules']['debug'] = [
		'class' => 'yii\debug\Module',
	];

	$config['bootstrap'][] = 'gii';
	$config['modules']['gii'] = [
		'class' => 'yii\gii\Module',
	];
}

return $config;
