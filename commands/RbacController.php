<?php
namespace app\commands;
 
use Yii;
use yii\console\Controller;
use app\rbac\Rbac as AdminRbac;
 
/**
 * RBAC generator
 */
class RbacController extends Controller
{
	/**
	 * Generates roles
	 */
	public function actionInit()
	{
		$auth = Yii::$app->getAuthManager();
		$auth->removeAll();
 
		$adminPanel = $auth->createPermission(AdminRbac::PERMISSION_USER_EDIT);
		$adminPanel->description = 'Редактирование пользователей';
		$auth->add($adminPanel);
		
		$contentEdit = $auth->createPermission(AdminRbac::PERMISSION_CONTENT_EDIT);
		$contentEdit->description = 'Редактирование контента';
		$auth->add($contentEdit);
		
 
		$user = $auth->createRole('user');
		$user->description = 'Покупатель';
		$auth->add($user);
 
		$admin = $auth->createRole('admin');
		$admin->description = 'Admin';
		$auth->add($admin);
		
		$writer = $auth->createRole('editor');
		$writer->description = 'Редактор';
		$auth->add($writer);

		$auth->addChild($admin, $writer);
		$auth->addChild($admin, $adminPanel);
		$auth->addChild($admin, $user);
		$auth->addChild($writer, $contentEdit);
 
		$this->stdout('Done!' . PHP_EOL);
	}
}
?>
