<?php

namespace app\modules\user\models;
use yii\web\IdentityInterface;
use app\modules\user\models\query\UserQuery;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $username
 * @property string $auth_key
 * @property string $email_confirm_token
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $defaultAdress
 * @property integer $status
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface
{
	const STATUS_BLOCKED = 0;
	const STATUS_ACTIVE = 1;
	const STATUS_WAIT = 2;

	/**
	 * @var int
	 */
	
	
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'user';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			['username', 'required'],
			['username', 'match', 'pattern' => '#^[\w_-]+$#i'],
			['username', 'unique', 'targetClass' => self::className(), 'message' => 'Имя пользователя уже занято.'],
			[['username', 'userView'], 'string', 'min' => 3, 'max' => 255],
			
			['email', 'required'],
			['email', 'email'],
			['email', 'string', 'max' => 255],
			['email', 'unique', 'targetClass' => self::className(), 'message' => 'Этот адрес уже используется'],
			
			[['status', 'personalDiscount'], 'integer'],
			['status', 'default', 'value' => self::STATUS_ACTIVE],
			['status', 'in', 'range' => array_keys(self::getStatusesArray())],

			['customerPhone', 'string', 'min' => 6, 'max' => 10, 'message' => 'Телефон без 8'],
			['personalDiscount', 'default', 'value' => 0],
			
			['role', 'string', 'max' => 60],
		];
	}
	public function getStatusName()
	{
		return ArrayHelper::getValue(self::getStatusesArray(), $this->status);
	}
 
	public static function getStatusesArray()
	{
		return [
			self::STATUS_BLOCKED => 'Заблокирован',
			self::STATUS_ACTIVE => 'Активен',
			self::STATUS_WAIT => 'Ожидает подтверждения',
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'created_at' => 'Создан',
			'personalDiscount' => 'Текущая скидка',
			'userView' => 'Имя пользователя',
			'updated_at' => 'Обновлен',
			'username' => 'Логин для входа',
			'email' => 'Электронная почта',
			'defaultAdress' => 'Адрес доставки',
			'status' => 'Статус',
			'customerPhone' => 'Телефон без 8',
			'role' => 'Уровень доступа',
		];
	}

	public function behaviors()
	{
		return [
			TimestampBehavior::className(),
		];
	}
	public static function findIdentity($id)
	{
		return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
	}
 
	public static function findIdentityByAccessToken($token, $type = null)
	{
		throw new NotSupportedException('Поиск по токену не выполнен.');
	}
 
	public function getId()
	{
		return $this->getPrimaryKey();
	}
 
	public function getAuthKey()
	{
		return $this->auth_key;
	}
 
	public function validateAuthKey($authKey)
	{
		return $this->getAuthKey() === $authKey;
	}
	/**
	 * Finds user by username
	 *
	 * @param string $username
	 * @return static|null
	 */
	public static function findByUsername($username)
	{
		return static::findOne(['username' => $username]);
	}
 
	/**
	 * Validates password
	 *
	 * @param string $password password to validate
	 * @return boolean if password provided is valid for current user
	 */
	public function validatePassword($password)
	{
		return Yii::$app->security->validatePassword($password, $this->password_hash);
	}
	/**
	 * @param string $password
	 */
	public function setPassword($password)
	{
		$this->password_hash = Yii::$app->security->generatePasswordHash($password);
	}
 
	/**
	 * Generates "remember me" authentication key
	 */
	public function generateAuthKey()
	{
		$this->auth_key = Yii::$app->security->generateRandomString();
	}
 
	public function beforeSave($insert)
	{
		if (parent::beforeSave($insert)) {
			if ($insert) {
				$this->generateAuthKey();
			}
			return true;
		}
		return false;
	}
	/**
	 * Finds user by password reset token
	 *
	 * @param string $token password reset token
	 * @return static|null
	 */
	public static function findByPasswordResetToken($token, $timeout)
	{
		if (!static::isPasswordResetTokenValid($token, $timeout)) {
			return null;
		}
		return static::findOne([
			'password_reset_token' => $token,
			'status' => self::STATUS_ACTIVE,
		]);
	}
 
	/**
	 * Finds out if password reset token is valid
	 *
	 * @param string $token password reset token
	 * @return boolean
	 */
	public static function isPasswordResetTokenValid($token, $timeout)
	{
		if (empty($token)) {
			return false;
		}
//		$expire = Yii::$app->params['user.passwordResetTokenExpire'];
		$parts = explode('_', $token);
		$timestamp = (int) end($parts);
		return $timestamp +  $timeout >= time();
	}
 
	/**
	 * Generates new password reset token
	 */
	public function generatePasswordResetToken()
	{
		$this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
	}
 
	/**
	 * Removes password reset token
	 */
	public function removePasswordResetToken()
	{
		$this->password_reset_token = null;
	}
	/**
	 * @param string $email_confirm_token
	 * @return static|null
	 */
	public static function findByEmailConfirmToken($email_confirm_token)
	{
		return static::findOne(['email_confirm_token' => $email_confirm_token, 'status' => self::STATUS_WAIT]);
	}
 
	/**
	 * Generates email confirmation token
	 */
	public function generateEmailConfirmToken()
	{
		$this->email_confirm_token = Yii::$app->security->generateRandomString();
	}
 
	/**
	 * Removes email confirmation token
	 */
	public function removeEmailConfirmToken()
	{
		$this->email_confirm_token = null;
	}
	/**
	 * @return UserQuery
	 */
	public static function find()
	{
		return Yii::createObject(UserQuery::className(), [get_called_class()]);
	}
}
