<?php
namespace app\modules\user\models\forms;
use app\modules\user\models\User;
use yii\base\Model;
use yii\db\ActiveQuery;
use Yii;

class ProfileUpdateForm extends Model
{
	public $email;
	public $userView;
	public $customerPhone;
	/**
	* @var User
	*/
	private $_user;
 
	public function __construct(User $user, $config = [])
	{
		$this->_user = $user;
		parent::__construct($config);
	}
 
	public function init()
	{
		$this->email = $this->_user->email;
		$this->userView = $this->_user->userView;
		$this->customerPhone = $this->_user->customerPhone;
		parent::init();
	}
 
	public function rules()
	{
		return [
			['email', 'required'],
			['email', 'email'],
			[
				'email',
				'unique',
				'targetClass' => User::className(),
				'message' => 'Такой адрес электронной почты уже используется',
				'filter' => function (ActiveQuery $query) {
						$query->andWhere(['<>', 'id', $this->_user->id]);
					},
			],
			[['email' ,'userView'], 'string', 'max' => 255],
			['customerPhone', 'integer' , 'min' => 100000],
		];
	}
 
	public function update()
	{
		if ($this->validate()) {
			$user = $this->_user;
			$user->email = $this->email;
			$user->userView = $this->userView;
			$user->customerPhone = $this->customerPhone;
			return $user->save();
		} else {
			return false;
		}
	}
	public function attributeLabels()
	{
		return [
			'userView' => 'Имя пользователя',
			'customerPhone' => 'Телефон без 8',
			'email' => 'Электронная почта',
		];
	}
}
?>
