<?php
namespace app\modules\user\models\forms;
use app\modules\user\models\User;
use yii\base\Model;
use yii\db\ActiveQuery;
use Yii;

class PasswordChangeForm extends Model
{
	public $currentPassword;
	public $newPassword;
	public $newPasswordRepeat;
	/**
	* @var User
	*/
	private $_user;
	/**
	 * @param User $user
	 * @param array $config
	*/
 
	public function __construct(User $user, $config = [])
	{
		$this->_user = $user;
		parent::__construct($config);
	}
 /*
	public function init()
	{
		$this->email = $this->_user->email;
		parent::init();
	}
 */
	public function rules()
	{
		return [
			[['currentPassword', 'newPassword', 'newPasswordRepeat'], 'required'],
			['currentPassword', 'validatePassword'],
			['newPassword', 'string', 'min' => 6],
			['newPasswordRepeat', 'compare', 'compareAttribute' => 'newPassword'],
		];
	}
 
	public function update()
	{
		if ($this->validate()) {
			$user = $this->_user;
			$user->email = $this->email;
			return $user->save();
		} else {
			return false;
		}
	}
	public function attributeLabels()
	{
		return [
			'newPassword' => 'Новый пароль',
			'newPasswordRepeat' => 'Новый пароль еще раз',
			'currentPassword' => 'Текущий пароль',
		];
	}
	/**
	 * @param string $attribute
	 * @param array $params
	 */
	public function validatePassword($attribute, $params)
	{
		if (!$this->hasErrors()) {
			if (!$this->_user->validatePassword($this->$attribute)) {
				$this->addError($attribute, 'Текущий пароль не такой');
			}
		}
	}
	/**
	 * @return boolean
	 */
	public function changePassword()
	{
		if ($this->validate()) {
			$user = $this->_user;
			$user->setPassword($this->newPassword);
			return $user->save();
		} else {
			return false;
		}
	}
}
?>
