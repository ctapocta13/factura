<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Вход';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-default-login">
	<h2><?= Html::encode($this->title) ?></h2>

	<p>Чтобы войти, пожалуйста введите следующее:</p>

	<?php $form = ActiveForm::begin([
		'id' => 'login-form',
		'options' => ['class' => 'form-horizontal'],
		'fieldConfig' => [
			'template' => "{label}\n<div class=\"col-lg-4\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
			'labelOptions' => ['class' => 'col-lg-3 control-label'],
		],
	]); ?>

		<?= $form->field($model, 'username') ?>

		<?= $form->field($model, 'password')->passwordInput() ?>

		<?= $form->field($model, 'rememberMe')->checkbox([
			'template' => "<div class=\"col-lg-offset-1 col-lg-3\">{input} {label}</div>\n<div class=\"col-lg-8\">{error}</div>",
		]) ?>
		<div style="color:#999;margin:1em 0">
			Если у вас нет учетной записи - вы можете <?= Html::a('зарегистрироваться', ['signup']) ?>.<br>
		</div>
		<div style="color:#999;margin:1em 0">
			Если вы забыли пароль - вы можете его <?= Html::a('сбросить', ['password-reset-request']) ?>.
		</div>
		<div class="form-group" style="color:#999;margin:1em 0">
			<div>
				<?= Html::submitButton('Войти', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
			</div>
		</div>
	<?php ActiveForm::end(); ?>
	<!-- Скидка
	<div style="color:#999;margin:1em 0">
		Для получения дополнительной скидки обратитесь к <a href="mailto:<?=Yii::$app->params['supportEmail']?>">администратору.</a><br>
	</div>
	-->
</div>
