<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$this->title = 'Смена пароля';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-default-request-password-reset">
	<h2><?= Html::encode($this->title) ?></h2>

	<p>Укажите Ваш адрес электронной почты. На него будет выслана ссылка для смены пароля.</p>

	<div class="row">
		<div class="col-lg-5">
			<?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>

				<?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

				<div class="form-group">
					<?= Html::submitButton('Отправить', ['class' => 'btn btn-primary']) ?>
				</div>

			<?php ActiveForm::end(); ?>
		</div>
	</div>
</div>
