<?php
use yii\captcha\Captcha;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
 
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\modules\user\models\SignupForm */
 
$this->title = 'Создание учетной записи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-default-signup">
	<h2><?= Html::encode($this->title) ?></h2>
 
	<p>Чтобы зарегистрироваться, пожалуйста введите следующее:</p>
 
	<div class="row">
		<div class="col-lg-5">
			<?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
			<?= $form->field($model, 'username') ?>
			<?= $form->field($model, 'email') ?>
			<?= $form->field($model, 'password')->passwordInput() ?>
			<?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
				'captchaAction' => '/user/default/captcha',
				'template' => '<div class="row"><div class="col-lg-4">{image}</div><div class="col-lg-5">{input}</div></div>',
			]) ?>
			<div class="form-group">
				<?= Html::submitButton('Создать', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
			</div>
			<?php ActiveForm::end(); ?>
		</div>
	</div>
</div>
