<?php
 
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
 
/* @var $this yii\web\View */
/* @var $model app\modules\user\models\ChangePasswordForm */
$this->title = 'Смена пароля';
$this->params['breadcrumbs'][] = ['label'=>'Личный кабинет', 'url' => ['/user/profile']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-profile-password-change">
	<h2><?= Html::encode($this->title) ?></h2>
	<div class="user-form">

		<?php $form = ActiveForm::begin(); ?>

		<?= $form->field($model, 'currentPassword')->passwordInput(['maxlength' => true]) ?>
		<?= $form->field($model, 'newPassword')->passwordInput(['maxlength' => true]) ?>
		<?= $form->field($model, 'newPasswordRepeat')->passwordInput(['maxlength' => true]) ?>
 
		<div class="form-group">
			<?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
		</div>
 
		<?php ActiveForm::end(); ?>
	 
	</div>
 
</div>
