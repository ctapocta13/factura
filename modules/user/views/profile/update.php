<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
 
/* @var $this yii\web\View */
/* @var $model app\modules\user\models\User */
 
$this->title = 'Редактирование профиля';
$this->params['breadcrumbs'][] = ['label'=>'Личный кабинет', 'url' => ['/shop/admin']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-profile-update">

	<h2><?= Html::encode($this->title) ?></h2>

	<div class="user-form">

		<?php $form = ActiveForm::begin(); ?>

		<?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
		<?= $form->field($model, 'userView')->textInput(['maxlength' => true]) ?>
		<?= $form->field($model, 'customerPhone')->textInput(['maxlength' => true]) ?>

		<div class="form-group">
			<?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
		</div>

		<?php ActiveForm::end(); ?>

	</div>

</div>
