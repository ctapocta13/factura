<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
/* @var $this yii\web\View */
/* @var $model app\modules\user\models\User */
 
$this->title = 'Личный кабинет';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-profile">
	<h2><?= Html::encode($this->title) ?></h2>
	<p>
		<?= Html::a('Редактировать профиль', ['update'], ['class' => 'btn btn-primary']) ?>
		<?= Html::a('Сменить пароль', ['password-change'], ['class' => 'btn btn-primary']) ?>
		<?= Html::a('История заказов', ['history'], ['class' => 'btn btn-primary disabled']) ?>
	</p>
	<?= DetailView::widget([
		'model' => $model,
		'attributes' => [
			'username',
			'userView',
			'personalDiscount',
			'customerPhone',
			'defaultAdress',
			'email',
			
		],
	]) ?>

</div>
