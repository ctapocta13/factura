<?php

namespace app\modules\user;

use yii\console\Application as ConsoleApplication;
use Yii;

class Module extends \yii\base\Module
{
	public $controllerNamespace = 'app\modules\user\controllers';
	/**
	 * @var int
	 */
	public $passwordResetTokenExpire = 3600;
	/**
	 * @var int
	 */
	public $emailConfirmTokenExpire = 259200; // 3 days
	/**
	 * @var string
	 */
	public $defaultRole = 'user';

	public function init()
	{
		parent::init();

		// custom initialization code goes here
/*		if (Yii::$app instanceof ConsoleApplication) {
			$this->controllerNamespace = 'app\modules\user\commands';
		}*/
	}
}
