<?php

namespace app\modules\user\controllers;

use app\modules\user\models\User;
use app\modules\user\models\forms\ProfileUpdateForm;
use app\modules\user\models\forms\PasswordChangeForm;

use yii\filters\AccessControl;

use yii\web\Controller;

use Yii;

class ProfileController extends Controller
{
	public function behaviors()
	{
		return [//Только авторизованные пользователи
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
		];
	}
	public function actionIndex()
	{
		return $this->render('index', [
			'model' => $this->findModel(),
		]);
	}
	private function findModel()
	{
		return User::findOne(Yii::$app->user->identity->getId());
	}
	public function actionUpdate()
	{
		$user = $this->findModel();
		$model = new ProfileUpdateForm($user);
	 
		if ($model->load(Yii::$app->request->post()) && $model->update()) {
			return $this->redirect(['index']);
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}public function actionHistory()
	{
		\Yii::$app->session->setFlash('info', 'История заказов пока не готова');
		return $this->redirect(['index']);
	}
	public function actionPasswordChange()
	{
		$user = $this->findModel();
		$model = new PasswordChangeForm($user);
 
		if ($model->load(Yii::$app->request->post()) && $model->changePassword()) {
			return $this->redirect(['index']);
		} else {
			return $this->render('passwordChange', [
				'model' => $model,
			]);
		}
	}
}
