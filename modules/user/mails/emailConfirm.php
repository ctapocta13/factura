<?php
use yii\helpers\Html;
 
/* @var $this yii\web\View */
/* @var $user app\modules\user\models\User */
 
$confirmLink = Yii::$app->urlManager->createAbsoluteUrl(['user/default/email-confirm', 'token' => $user->email_confirm_token]);
?>
<h2>Здравствуйте, <?= Html::encode($user->username) ?>!</h2>
<p>Для подтверждения адреса пройдите по ссылке:</p>
<?= Html::a(Html::encode($confirmLink), $confirmLink) ?>
<p>Ссылка действительна в течении трех дней</p>
<p>Если Вы не регистрировались у на нашем сайте, то просто удалите это письмо.</p>
