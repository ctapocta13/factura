<?php
use yii\helpers\Html;
 
/* @var $this yii\web\View */
/* @var $user app\modules\user\models\User */
 
$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['user/default/password-reset', 'token' => $user->password_reset_token]);
?>
<h2>Здравствуйте, <?= Html::encode($user->username) ?>!</h2>
<p>Пройдите по ссылке, чтобы сменить пароль:</p>
<?= Html::a(Html::encode($resetLink), $resetLink) ?>
