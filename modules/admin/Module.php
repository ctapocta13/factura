<?php

namespace app\modules\admin;
use yii\filters\AccessControl;
use app\rbac\Rbac as AdminRbac;

class Module extends \yii\base\Module
{
	public $controllerNamespace = 'app\modules\admin\controllers';
	/*public $layout = 'app/edit';*/

	public function init()
	{
		parent::init();

		// custom initialization code goes here
	}
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'roles' => [AdminRbac::PERMISSION_USER_EDIT],
					],
				],
			],
		];
	}
}
