<?php

namespace app\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
//use app\modules\user\models\User;

/**
 * userSearch represents the model behind the search form about `app\modules\user\models\User`.
 */
class userSearch extends Model
{
	public $id;
	public $username;
	public $email;
	public $status;
	public $role;
	public $customerPhone;
	public $personalDiscount;
	public $date_from;
	public $date_to;
	
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['id', 'status', 'personalDiscount'], 'integer'],
			[['role', 'customerPhone'], 'string'],
			[['username', 'email'], 'safe'],
			[['date_from', 'date_to'], 'date', 'format' => 'php:Y-m-d'],
		];
	}
	public function attributeLabels()
		{
			return [
				'id' => 'ID',
				'created_at' => 'Дата регистрации',
				'updated_at' => 'Дата редактирования',
				'username' => 'Логин',
				'email' => 'Электронная почта',
				'status' => 'Статус',
				'date_from' => 'Начальная дата',
				'date_to' => 'Конечная дата',
				'role' => 'Уровень доступа',
				'customerPhone' => 'Телефон без 8',
				'personalDiscount' => 'Скидка',
			];
		}
	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$query = User::find();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => [
				'defaultOrder' => ['id' => SORT_DESC],
			]
		]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			$query->where('0=1');
			return $dataProvider;
		}

		$query->andFilterWhere([
			'id' => $this->id,
			'status' => $this->status,
			'personalDiscount' => $this->personalDiscount,
			'role' => $this->role,
		]);

		$query->andFilterWhere(['like', 'username', $this->username])
			->andFilterWhere(['like', 'email', $this->email])
			->andFilterWhere(['like', 'customerPhone', $this->customerPhone]);
		$query
			->andFilterWhere(['>=', 'created_at', $this->date_from ? strtotime($this->date_from . ' 00:00:00') : null])
			->andFilterWhere(['<=', 'created_at', $this->date_to ? strtotime($this->date_to . ' 23:59:59') : null]);
/*		$query->andFilterWhere([
			'id' => $this->id,
			'status' => $this->status,
			'role' => $this->role,
		]);
*/
		return $dataProvider;
	}
}
