<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\modules\admin\models\User;
/* @var $this yii\web\View */
/* @var $model app\modules\user\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

	<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
	<?= $form->field($model, 'customerPhone')->textInput(['maxlength' => true]) ?>
	<?= $form->field($model, 'newPassword')->passwordInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'newPasswordRepeat')->passwordInput(['maxlength' => true]) ?>
	<?= $form->field($model, 'personalDiscount')->textInput() ?>

	<?= $form->field($model, 'status')->dropDownList(User::getStatusesArray()) ?>
	<?= $form->field($model, 'role')->dropDownList(ArrayHelper::map(Yii::$app->authManager->getRoles(), 'name', 'description')) ?>

	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
