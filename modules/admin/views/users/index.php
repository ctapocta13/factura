<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\components\grid\ActionColumn;
use app\components\grid\SetColumn;
use app\components\grid\RoleColumn;
use app\components\grid\LinkColumn;
use app\modules\admin\models\User;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
//use Yii;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\userSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = ['label'=>'Управление магазином', 'url' => ['/shop/admin']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

	<h2><?= Html::encode($this->title) ?></h2>
	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<p>
		<?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
	</p>

	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'columns' => [
			[
				'filter' => DatePicker::widget([
					'model' => $searchModel,
					'attribute' => 'date_from',
					'attribute2' => 'date_to',
					'type' => DatePicker::TYPE_RANGE,
					'separator' => '-',
					'pluginOptions' => ['format' =>'yyyy-mm-dd']
				]),
				'attribute' => 'created_at',
				'format' => 'datetime',
			],
			
			[
				'class' => LinkColumn::className(),
				'attribute' => 'username',
			],
			'email:email',
			'customerPhone',
			'personalDiscount',
/*			[
				'attribute' =>'priceColumn',
				'class' =>SetColumn::className(),
				'filter' => [1=>1,2=>2,3=>3,4=>4],
				'cssCLasses' => [
					1 => 'success',
					2 => 'warning',
					3 => 'warning',
					4 => 'danger',
					5 => 'danger',
				],
			],*/
			
			[
				'class' =>SetColumn::className(),
				'filter' => User::getStatusesArray(),
				'attribute' => 'status',
				'name' => 'statusName',
				'cssCLasses' => [
					User::STATUS_ACTIVE => 'success',
					User::STATUS_WAIT => 'warning',
					User::STATUS_BLOCKED => 'default',
				],
			],
			[
			'class' => RoleColumn::className(),
			'filter' => ArrayHelper::map(Yii::$app->authManager->getRoles(), 'name', 'description'),
			'attribute' => 'role',
		],

			['class' => ActionColumn::className()],
		],
	]); ?>

</div>
