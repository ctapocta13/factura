<?php

namespace app\modules\shop\controllers;

use app\modules\shop\models\forms\OrderForm;
use app\components\Basket;
//use yii\data\ActiveDataProvider;

//use yii\helpers\ArrayHelper;
use Yii;
use yii\web\Controller;

class CheckoutController extends Controller
{
	
	public function actionOrder(){
		$model = new OrderForm();
		if ($model->load(Yii::$app->request->post())){
			$model->contact(Yii::$app->params['ordersEmail']); 
			\Yii::$app->session->setFlash('model', $model);

			return $this->redirect('thankyou');
		}
		$basket = new Basket();
		if (count($basket->products)==0) {
			\Yii::$app->session->setFlash('error', 'Заказ не может быть пустым');
			return $this->goHome();
		}
		return $this->render('order', [
			'model' => $model,
		]);
	}
	public function actionThankyou(){
		$model=\Yii::$app->session->getFlash('model', false);
		if ($model===false) {
			\Yii::$app->session->setFlash('error', 'Попытка обмануть систему не удалась :-/');
			return $this->goHome();
		}
		return $this->render('thankYou', [
			'model' => $model,
		]);
	}
	
	public function actionIndex(){
		
		$model=new Basket();
		$model->syncPrice();
		return $this->render('index', [
			'model'=>$model,
		]);
	}
	public function actionCheckoutPjax($id, $delta){
		$basket = new Basket();
		if ($delta>0) $basket->addIntoBasket($id);
		else $basket->substractFromBasket($id);
		
		return $this->render('index', [
			'model'=>$basket,
		]);
	}
}
?>
