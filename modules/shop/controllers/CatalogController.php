<?php

namespace app\modules\shop\controllers;

use app\modules\shop\models\Products;

use yii\data\ActiveDataProvider;
use yii\web\Controller;

class CatalogController extends Controller
{
	public function actionIndex($id='')
	{
		$products = new Products();
		$cats=($id!='') ? explode('/', $id) : $id;		//Разбили путь на массив
		$query=$products->find()->toShow()->toPath($cats);		//методы модели
		$dataProvider=new ActiveDataProvider([
			'query' => $query->orderBy(['productsIsAction'=>SORT_DESC]),
			'pagination' => [
				'pageSize' => 24,
			],
		]);
		return $this->render('index',[
			'dataProvider' => $dataProvider,
			'cats'=>$cats,
			'path'=>$id,
		]);
	}
}
?>
