<?php
namespace app\modules\shop\controllers\admin;

use Yii;
use app\components\widgets\slider\models\Slider;
use app\components\widgets\slider\models\SliderSearch;
use yii\web\Controller;
use yii\helpers\Url;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\rbac\Rbac as AdminRbac;

/**
 * SliderController implements the CRUD actions for Slider model.
 */
class SliderController extends Controller
{
	public function behaviors()
	{
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['POST'],
				],
			],
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'roles' => [AdminRbac::PERMISSION_CONTENT_EDIT],
					],
				],
			],
		];
	}
	/**
	 * Lists all Slider models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new SliderSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	/**
	 * Displays a single Slider model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id)
	{
		return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	/**
	 * Creates a new Slider model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new Slider();
		
		if ($model->load(Yii::$app->request->post())){
			$model->saveNewPicture(UploadedFile::getInstance($model, 'newPicture'));
			$model->save();
			return $this->redirect(['view', 'id' => $model->id]);
		}
		else {
			
			return $this->render('create', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Updates an existing Slider model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);

		if ($model->load(Yii::$app->request->post()) ) {
			
			$model->saveNewPicture(UploadedFile::getInstance($model, 'newPicture'));
			$model->save();
			return $this->redirect(['view', 'id' => $model->id]);
		}
		else {
			$model->startActivity=date('d.m.Y',$model->startActivity);
			$model->stopActivity=date('d.m.Y',$model->stopActivity);
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing Slider model and file assotiated with object.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id)
	{
		$model=$this->findModel($id);
		$model->deleteOldPicture();/*
		$filename = $_SERVER['DOCUMENT_ROOT'].Url::to("@web/images/slider/".$model->photoPath);
		
		if (file_exists($filename))
			if (!unlink($filename)) \Yii::$app->session->setFlash('error', 'Не могу удалить связанный файл!');*/
		$model->delete();

		return $this->redirect(['index']);
	}

	/**
	 * Finds the Slider model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Slider the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = Slider::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('Запрошенная страница не существует.');
		}
	}
}
