<?php

namespace app\modules\shop\controllers\admin;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use app\rbac\Rbac as AdminRbac;

class CatalogController extends Controller
{
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'roles' => [AdminRbac::PERMISSION_CONTENT_EDIT],
					],
				],
			],
		];
	}
	public function actionIndex()
	{
		return $this->render('index');
	}
	public function actionUpdate()
	{
		$objPHPExcel = new \PHPExcel();
		return $this->render('index');
	}
}
