<?php

namespace app\modules\shop\controllers;

use app\components\Basket;

use yii\web\Controller;

class DefaultController extends Controller
{
	public function actionIndex()
	{
		return $this->redirect('catalog');
	}
	
	public function actionAddBasket($id)
	{
		$basket = new Basket();
		return $basket->addIntoBasket($id);
	}
	
	public function actionUpdateBasket(){
		$basket=new Basket;
		return json_encode($basket->getState());
	}
	
	public function actionCleanBasket(){
		$basket=new Basket();
		$basket->clearBasket();
		return $this->goHome();
	}
}
