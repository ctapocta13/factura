<?php

namespace app\modules\shop\controllers;

use app\modules\shop\models\forms\UploadForm;
use yii\web\UploadedFile;

use yii\data\ActiveDataProvider;
use app\components\Basket;
use yii\filters\AccessControl;
use app\rbac\Rbac as AdminRbac;

use Yii;
use yii\web\Controller;

use app\modules\shop\OrdersH;

class AdminController extends Controller
{
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'roles' => [AdminRbac::PERMISSION_CONTENT_EDIT],
					],
				],
			],
		];
	}
	public function actionIndex()
	{
		return $this->render('index');
	}
	public function actionTest()
	{
		return $this->render('test');
	}
	
	//dev only
	public function actionDirtBasket($id='clean'){
		$basket=new Basket();
		$basket->dirtBasket();
		return $this->render('dev');
	}
	//dev only
	public function actionView()
	{
		$request = Yii::$app->request;
		switch ($request->get('id')){
			case 'products':
				$searchModel = new Products();
				break;
			case 'brands':
				$searchModel = new Brands();
				break;
			case 'units':
				$searchModel = new Units();
				break;
			case 'manufacturers':
				$searchModel = new Manufacturers();
				break;
			case 'categories':
				$searchModel = new Categories();
				break;
			default:
				$searchModel = new Products();
		}
		$query=$searchModel->find();
		$dataProvider=new ActiveDataProvider([
			'query' => $query,
		]);
		return $this->render('view',[
			'dataProvider' => $dataProvider,
		]);
	}
	public function actionUpload(){
		$model = new UploadForm();
		if ($model->load(Yii::$app->request->post())){
			$model->fileToUpload=UploadedFile::getInstance($model, 'fileToUpload');
			$model->loadFromCsv();
//			\Yii::$app->session->setFlash('upload', $model);
			return $this->render('uploadResult', [
				'model'=>$model,
			]);
		}
		
		return $this->render('upload', [
			'model' => $model,
		]);
	}
	public function actionLoad()
	{
		$request = Yii::$app->request;
		switch ($request->get('id')){
			case 'products':
				$searchModel = new Products();
				break;
			case 'brands':
				$searchModel = new Brands();
				break;
			case 'units':
				$searchModel = new Units();
				break;
			case 'manufacturers':
				$searchModel = new Manufacturers();
				break;
			case 'categories':
				$searchModel = new Categories();
				break;
			default:
				$searchModel = new Products();
		}
		
		$errors=$searchModel->LoadFromCsv($request->get('id').'.csv');
		$query=$searchModel->find();
		$dataProvider=new ActiveDataProvider([
			'query' => $query,
		]);
		return $this->render('view',[
			'dataProvider' => $dataProvider,
			'errors'=>$errors,
		]);
	}
	/*
 public function saveIntoCSV($filename)
	{
		$this->fileObject = new SplFileObject($filename, 'w');
		$this->fileObject->fputcsv(array_keys($this->attributes),'|');
		foreach($this->find()->all() as $currentRec)
		{
			$this->fileObject->fputcsv($currentRec->attributes,'|');
		}
		return true;
	}
*/
}
?>
