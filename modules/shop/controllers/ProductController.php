<?php

namespace app\modules\shop\controllers;
use app\modules\shop\models\Products;
use yii\web\Controller;

class ProductController extends Controller
{
	
	public function actionIndex($id)
	{
		$products = new Products();

		return $this->render('index',[
			'product'=>$products->findOne($id),
		]);
	}
}
?>
