<?php

use yii\helpers\Html;
use yii\helpers\Url;


use app\components\widgets\TreeWidget;

use yii\widgets\ListView;

use app\modules\shop\models\Categories;

/* @var $this yii\web\View */

$this->title = 'Каталог';
$catalog='/shop/catalog';

//$this->params['breadcrumbs'][] = ['label'=>'Каталог', 'url' => [$catalog, 'id' =>'']];
if ($path!='') {
	$cat=explode('/', $path);
	$path2='';
	for($i=0;$i<count($cats);$i++) {
		$subPath=Categories::findOne($cat[$i]);
		$path2.=$cat[$i].'/';
		$this->params['breadcrumbs'][] = ['label' => $subPath['categoriesShortName'], 'url' => [$catalog, 'id' => $path2]];

	}
}
?>
<div class="shop-catalog-index">
	<h2><?=$this->title?></h2>
	<!--<h3>Можно выводить информацию к ветке дерева</h3>-->
	<hr/>
		<div class="row">
			<div class="tree col-lg-3 col-sm-3 col-md-3 col-xs-3">
				<?=TreeWidget::widget()?>
			</div>
			<div class="col-lg-9 col-sm-9 col-md-9 col-sm">
					<?= ListView::widget([
						'dataProvider' => $dataProvider,
						'itemView'=>'productPreview',
						//'emptyText'=>'Товар в данной категории отсутствует',
					]) ?>
					
			</div>
		</div>
</div>
