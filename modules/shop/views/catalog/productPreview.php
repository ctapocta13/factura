<?php
use yii\helpers\Html;
use app\modules\shop\models\Products;
//productPreview. item element for ListItem of index

?>
<div class="product-preview col-lg-3 col-sm-6 col-md-4 col-xs-6">
	<div class='product-image-wrapper product-image-wrapper-preview img-responsive'>
		<?= Html::a(
				Html::img($model->getPhotoLink(Products::PHOTO_PREVIEW),[
					'class'=>'product-image',
					'alt'=>$model->getAlt(),
				]),
			['/shop/product','id'=>$model->productsSlug])?>
	</div>
	<div class="product-name">
		<p ><?=$model->getShortName()?></p>
	</div>
	<div class="product-price">
		<div>
			<strong class="pull-left"><?=$model->productsIsSQU ? 'От ' : ''?><?=$model->getPrice()?> руб.</strong>
		</div>
		<?php if ($model->productsIsSQU):?>
			<div class='product-into-basket'>
				<?= Html::a(
				'Подробнее',
				['/shop/product','id'=>$model->productsSlug], ['class'=>'btn btn-success pull-right'])?>
			</div>
			
		<?php else:?>
			<div class='product-into-basket'>
			<a class="btn btn-success pull-right intoBasket" data-productid="<?=$model->productsSlug?>" >В корзину</a>
		</div>
		<?php endif?>
	</div>

	<?php if ($model->productsIsAction):?>
		<div class="product-action btn btn-danger">Акция!</div>
	<?php endif?>
	<?php if ($model->productsActionDiscount):?>
		<div class="product-action-discount btn btn-info">-<?=$model->productsActionDiscount?>%</div>
	<?php endif?>
</div>
