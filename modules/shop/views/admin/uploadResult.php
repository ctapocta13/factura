<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
/* @var $this yii\web\View */

$this->title = 'Результат загрузки файла';
$this->params['breadcrumbs'][] = ['label'=>'Управление магазином', 'url' => ['/shop/admin']];
$this->params['breadcrumbs'][] = $this->title;
$class="col-lg-4 col-md-4 col-sm-4 col-xs-4";
?>
<div class="shop-admin-uploadResult">
	<h2><?=$this->title.' '.$model->filename?></h2>
	<div class="alert alert-info" role="alert">
		<h3>Информация</h3><hr/>
		<div class="row">
			<strong class="<?=$class?>">Имя таблицы:</strong>
			<div class="<?=$class?>"><?=$model->table?></div>
		</div>
		<div class="row">
			<strong class="<?=$class?>">Количество полей:</strong>
			<div class="<?=$class?>"><?=$model->columnCount?></div>
		</div>
		<div class="row">
			<strong class="<?=$class?>">Строк с обработано:</strong>
			<div class="<?=$class?>"><?=$model->counters['rowsParsed']?></div>
		</div>
		<div class="row">
			<strong class="<?=$class?>">Ошибок обработки:</strong>
			<div class="<?=$class?>"><?=$model->counters['errorParsing']?></div>
		</div>
		<div class="row">
			<strong class="<?=$class?>">Строк обновлено:</strong>
			<div class="<?=$class?>"><?=$model->counters['rowsUpdated']?></div>
		</div>
		<div class="row">
			<strong class="<?=$class?>">Строки без изменений:</strong>
			<div class="<?=$class?>"><?=$model->counters['rowsUpToDate']?></div>
		</div>
		<div class="row">
			<strong class="<?=$class?>">Строк вставлено:</strong>
			<div class="<?=$class?>"><?=$model->counters['rowsInserted']?></div>
		</div>
	</div>
	<?php if (is_array($model->errors)):?>
		<div class="alert alert-danger" role="alert">
			<h3>Ошибки при загрузке</h3><hr/>
			<table class="table table-striped table-bordered">
			<tr><th>Строка</th><th>Колонка</th><th>Сообщение</th></tr>
			<?php foreach($model->errors as $key=>$error):?>
				<?php $i=0;?>
				<?php foreach ($error as $col=>$colError):?>
					<tr>
						<?=!$i++ ? '<td rowspan="'.count($error).'">'.$key.'</td>' : ''?>
						<td><?=$col?></td>
						<td>
							<ul>
								<?php foreach($colError as $listError):?>
									<li><?=$listError?></li>
								<?php endforeach?>
							</ul>
						</td>
					</tr>
				<?php endforeach?>
			<?php endforeach?>
			</table>
		</div>
	<?php endif?>
	<?= Html::a('Загрузить еще', ['upload'], ['class' => 'btn btn-success']) ?>
</div>
