<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\shop\models\Units */

$this->title = 'Просмотр '.$model->unitsSlug;
$this->params['breadcrumbs'][] = ['label' => 'Управление магазином', 'url' => ['/shop/admin']];
$this->params['breadcrumbs'][] = ['label' => 'Единицы измерения', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Просмотр '.$model->unitsSlug;
?>
<div class="units-view">

	<h2><?= Html::encode($this->title) ?></h2>

	<p>
		<?= Html::a('Редактировать', ['update', 'id' => $model->unitsSlug], ['class' => 'btn btn-primary']) ?>
	</p>

	<?= DetailView::widget([
		'model' => $model,
		'attributes' => [
			'unitsSlug',
			'unitsFullName',
			'unitsShortName',
		],
	]) ?>

</div>
