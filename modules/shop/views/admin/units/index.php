<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\shop\models\UnitsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Единицы измерения';
$this->params['breadcrumbs'][] = ['label'=>'Управление магазином', 'url' => ['/shop/admin']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="units-index">

	<h2><?= Html::encode($this->title) ?></h2>
	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<p>
		<?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
	</p>

	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],

			'unitsSlug',
			'unitsFullName',
			'unitsShortName',

			[
				'class' => 'yii\grid\ActionColumn',
				'template' => '{view} {update}',
			],
		],
	]); ?>

</div>
