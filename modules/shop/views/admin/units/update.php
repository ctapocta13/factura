<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\shop\models\Units */

$this->title = 'Редактирование: ' . ' ' . $model->unitsSlug;
$this->params['breadcrumbs'][] = ['label' => 'Управление магазином', 'url' => ['/shop/admin']];
$this->params['breadcrumbs'][] = ['label' => 'Единицы измерения', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->unitsSlug, 'url' => ['view', 'id' => $model->unitsSlug]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="units-update">

	<h2><?= Html::encode($this->title) ?></h2>

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
