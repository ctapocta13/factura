<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\shop\models\Categories */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="categories-form">

	<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'categoriesSlug')->textInput(['disabled' => $model->isNewRecord ? false : true]) ?>

	<?= $form->field($model, 'categoriesFullName')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'categoriesShortName')->textInput(['maxlength' => true]) ?>

	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
