<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\shop\models\Categories */

$this->title = $model->categoriesSlug;
$this->params['breadcrumbs'][] = ['label' => 'Управление магазином', 'url' => ['/shop/admin']];
$this->params['breadcrumbs'][] = ['label' => 'Категории', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categories-view">

	<h2><?= Html::encode($this->title) ?></h2>

	<p>
		<?= Html::a('Редактировать', ['update', 'id' => $model->categoriesSlug], ['class' => 'btn btn-primary']) ?>
	</p>

	<?= DetailView::widget([
		'model' => $model,
		'attributes' => [
			'categoriesSlug',
			'categoriesFullName',
			'categoriesShortName',
		],
	]) ?>

</div>
