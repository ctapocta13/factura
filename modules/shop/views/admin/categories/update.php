<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\shop\models\Categories */

$this->title = 'Редактирование категории: ' . ' ' . $model->categoriesSlug;
$this->params['breadcrumbs'][] = ['label' => 'Управление магазином', 'url' => ['/shop/admin']];
$this->params['breadcrumbs'][] = ['label' => 'Категории', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->categoriesSlug, 'url' => ['view', 'id' => $model->categoriesSlug]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="categories-update">

	<h2><?= Html::encode($this->title) ?></h2>

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
