<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\shop\models\CategoriesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="categories-search">

	<?php $form = ActiveForm::begin([
		'action' => ['index'],
		'method' => 'get',
	]); ?>

	<?= $form->field($model, 'categoriesSlug') ?>

	<?= $form->field($model, 'categoriesFullName') ?>

	<?= $form->field($model, 'categoriesShortName') ?>

	<div class="form-group">
		<?= Html::submitButton('Искать', ['class' => 'btn btn-primary']) ?>
		<?= Html::resetButton('Сбросить', ['class' => 'btn btn-default']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
