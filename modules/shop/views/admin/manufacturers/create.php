<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\shop\models\Manufacturers */

$this->title = 'Создание производителя';
$this->params['breadcrumbs'][] = ['label' => 'Управление магазином', 'url' => ['/shop/admin']];
$this->params['breadcrumbs'][] = ['label' => 'Производители', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="manufacturers-create">

	<h2><?= Html::encode($this->title) ?></h2>

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
