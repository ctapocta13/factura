<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\shop\models\Manufacturers */

$this->title = 'Редактирование производителя: ' . ' ' . $model->manufacturersSlug;
$this->params['breadcrumbs'][] = ['label' => 'Управление магазином', 'url' => ['/shop/admin']];
$this->params['breadcrumbs'][] = ['label' => 'Производители', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->manufacturersSlug, 'url' => ['view', 'id' => $model->manufacturersSlug]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="manufacturers-update">

	<h2><?= Html::encode($this->title) ?></h2>

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
