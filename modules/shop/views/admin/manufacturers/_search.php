<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\shop\models\ManufacturersSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="manufacturers-search">

	<?php $form = ActiveForm::begin([
		'action' => ['index'],
		'method' => 'get',
	]); ?>

	<?= $form->field($model, 'manufacturersSlug') ?>

	<?= $form->field($model, 'manufacturersFullName') ?>

	<?= $form->field($model, 'manufacturersShortName') ?>

	<div class="form-group">
		<?= Html::submitButton('Искать', ['class' => 'btn btn-primary']) ?>
		<?= Html::resetButton('Сбросить', ['class' => 'btn btn-default']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
