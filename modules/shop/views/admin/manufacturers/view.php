<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\shop\models\Manufacturers */

$this->title = $model->manufacturersSlug;
$this->params['breadcrumbs'][] = ['label' => 'Управление магазином', 'url' => ['/shop/admin']];
$this->params['breadcrumbs'][] = ['label' => 'Производители', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="manufacturers-view">

	<h2><?= Html::encode($this->title) ?></h2>

	<p>
		<?= Html::a('Редактировать', ['update', 'id' => $model->manufacturersSlug], ['class' => 'btn btn-primary']) ?>
	</p>

	<?= DetailView::widget([
		'model' => $model,
		'attributes' => [
			'manufacturersSlug',
			'manufacturersPhoto',
			'manufacturersFullName',
			'manufacturersShortName',
		],
	]) ?>

</div>
