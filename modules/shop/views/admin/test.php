<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

//use app\modules\shop\models\Units;
//use yii\widgets\ListView;
//use app\components\widgets\BasketWidget;
//use yii\widgets\Pjax;
//use app\modules\shop\models\Products;
//use app\modules\shop\models\Brands;

/* @var $this yii\web\View */

$this->title = 'Test page';
$this->params['breadcrumbs'][] = ['label'=>'Управление магазином', 'url' => ['/shop/admin']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-test">
	<h2><?=$this->title?></h2>
	<p><a class="btn btn-success intoBasket" data-productid="europlast_cronshtein_1_19_002_1">Вывести id</a></p>
	<p><a class="btn btn-success intoBasket" data-productid="europlast_cronshtein_1_19_006_1">Рыба</a></p>
	
	<hr/>
	<?php 
		print_r(Brands::getFullList());
	?>
</div>
