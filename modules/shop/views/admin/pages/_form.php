<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use vova07\imperavi\Widget;

/* @var $this yii\web\View */
/* @var $model app\components\widgets\pages\models\Pages */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pages-form">

	<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'name')
		->textInput(['maxlength' => true])
		->hint('Используется только для удобства. Ни на что не влияет')
		 ?>

	<?= $form->field($model, 'body')->widget(Widget::className(), [
		'settings' => [
			'lang' => 'ru',
			'minHeight' => 400,
			'plugins' => [
				'fontfamily',
				'fontsize',
				'fontcolor',
				'table',
				//'textexpander',
				'fullscreen',
			]
		]
	]);
	?>


	<div class="form-group">
		<?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
