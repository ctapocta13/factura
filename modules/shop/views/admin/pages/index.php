<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\components\widgets\pages\models\PagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Страницы';
$this->params['breadcrumbs'][] = ['label' => 'Управление магазином', 'url' => ['/shop/admin']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="pages-index">

	<h2><?= Html::encode($this->title) ?></h2>
<?php Pjax::begin(); ?>    <?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'columns' => [
			[
				'class' => 'yii\grid\SerialColumn',
				'headerOptions' => ['width' => '50'],
			],

			'name',

			[
				'class' => 'yii\grid\ActionColumn',
				'headerOptions' => ['width' => '50'],
				'template' => '{update}{view}',

			],
		],
	]); ?>
<?php Pjax::end(); ?></div>
