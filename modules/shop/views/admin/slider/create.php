<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\shop\models\Slider */

$this->title = 'Добавить кадр';
$this->params['breadcrumbs'][] = ['label' => 'Управление магазином', 'url' => ['/shop/admin']];
$this->params['breadcrumbs'][] = ['label' => 'Слайдер', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="slider-create">

	<h2><?= Html::encode($this->title) ?></h2>

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
