<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use app\components\widgets\slider\models\SliderName;

/* @var $this yii\web\View */
/* @var $model app\modules\shop\models\Slider */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="slider-form">

	<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
	<div class='row'>
		<div class='col-lg-offset-1 col-lg-10'>
			<?php if ($model->photoPath!='') echo $model->photoPath=='' ? 
				'' : 
				Html::img('@web/images/slider/'.$model->photoPath, ['class'=>'img-responsive']);?>
		</div>
	</div>
	
	<div class='row'>
		<div class='col-lg-12'>
			<?= $form->field($model, 'newPicture')->fileInput() ?>
		</div>
	</div>
	<div class='row'>
		<div class='col-lg-12'>
			<?= $form->field($model, 'link')
			->hint('Ссылка на которую ведет слайд, например <strong>www.site/catalog/wear</strong>. # не ведет никуда и убирает ссылку у слайда')
			->textInput(['maxlength' => true]) ?>
		</div>
	</div>

	<?php //echo $form->field($model, 'photoPath')->textInput(['maxlength' => true]) ?>
	

	<div class='row'>
		<div class='col-lg-3'>
			<?= $form->field($model, 'sliderId')->dropDownList(SliderName::getList())?>
		</div>
		<div class='col-lg-3'>
			<?= $form->field($model, 'sortOrder')->hint('Число для сортировки слайдов')->textInput() ?>
		</div>
		<div class='col-lg-3'>
			<?= $form->field($model, 'startActivity')->widget(
				DatePicker::classname(),[
					'type' => 3 /*TYPE_COMPONENT_APPEND*/,
					'options' => ['placeholder' => 'Начало показа'],
					'pluginOptions' => [
						'autoclose' => true,
						'format' => 'dd.mm.yyyy',
//						'format' => 'php:U',
						'todayHighlight' => true,
						'startDate' => date('d-m-Y', strtotime('-1 days')),
			]]);?>
		</div>
		<div class='col-lg-3'>
			<?= $form->field($model, 'stopActivity')->widget(
				DatePicker::classname(),[
					'type' => 3 /*TYPE_COMPONENT_APPEND*/,
					'options' => ['placeholder' => 'Окончание показа'],
					'pluginOptions' => [
						'format' => 'dd.mm.yyyy',
						'autoclose' => true,
						'todayHighlight' => true,
						'startDate' => date('d-m-Y', strtotime('+1 days')),
			]])?>
		</div>
	</div>
	<?= $form->field($model, 'status')->checkbox([
		'template' => "<div class=\"col-lg-offset-1 col-lg-3\">{input} {label}</div>\n<div class=\"col-lg-8\">{error}</div>",
	]) ?>
	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Изменить', [
			'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'])
		?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
