<?php
//use app\components\grid\PictureColumn;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\shop\models\Slider */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Управление магазином', 'url' => ['/shop/admin']];
$this->params['breadcrumbs'][] = ['label' => 'Слайдер', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="slider-view">

	<h2><?= Html::encode($this->title) ?></h2>

	<p>
		<?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
		<?= Html::a('Удалить', ['delete', 'id' => $model->id], [
			'class' => 'btn btn-danger',
			'data' => [
				'confirm' => 'Вы уверены что хотите удалить элемент?',
				'method' => 'post',
			],
		]) ?>
	</p>

	<?= DetailView::widget([
		'model' => $model,
		'attributes' => [

			[
				'label' => 'Просмотр',
				'value' => $model->photoPath=='' ? '' : Html::img('@web/images/slider/'.$model->photoPath, ['class'=>'img-responsive']),//$model->parentName,
				'format' => 'html',
			],
			'photoPath',
			[
				'label' => 'Принадлежность',
				'value' => $model->parentName,
			],
			'link',
			[
				'label' => 'Показывать',
				'value' => $model->status ? 'Да' : 'Нет',
			],
			'sortOrder',
			[
				'attribute' => 'startActivity',
				'format' => ['date', 'php:d.m.Y'],
			],
			[
				'attribute' => 'stopActivity',
				'format' => ['date', 'php:d.m.Y'],
			],
			
		],
	]) ?>

</div>
