<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\date\DatePicker;
use app\components\grid\PictureColumn;
use app\components\widgets\slider\models\SliderName;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\shop\models\SliderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Слайдер';
$this->params['breadcrumbs'][] = ['label' => 'Управление магазином', 'url' => ['/shop/admin']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admin-slider-index">
	<h2><?= Html::encode($this->title) ?></h2>
	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>
	<p>
		<?= Html::a('Добавить кадр', ['create'], ['class' => 'btn btn-success']) ?>
	</p>
	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],
			[	//Миниатюра
				'attribute' =>'photoPath',
				'path' => '@web/images/slider/',
				'class' =>PictureColumn::className(),
				'headerOptions' => ['width' => '450'],
			],

			'link',
			[
				'attribute' =>'sortOrder',
				'headerOptions' => ['width' => '80'],
				'filter' => false,
			],
			[
				'label'=>'Слайдер',
				'attribute' =>'sliderId',
				'content'=>function($data){
					return $data->getParentName();
				},
				'filter' => SliderName::getList(),
				'headerOptions' => ['width' => '200'],
			],
			[
				'attribute' => 'status',
				'filter' => [1 => 'Да',0 => 'Нет'],
				'content'=>function($model, $key, $index, $grid){
					$value = $grid->getDataCellValue($model, $key, $index);
					return $value==0 ? 'Нет' : 'Да';
				},
				'headerOptions' => ['width' => '80'],
			],
			[
				'filter' => DatePicker::widget([
					'model' => $searchModel,
					'attribute' => 'startActivity',
					'type' => DatePicker::TYPE_INPUT,
				]),

				'attribute' => 'startActivity',
				'format' => ['date', 'php:d.m.Y'],
				'headerOptions' => ['width' => '240'],
			],
			[
				'filter' => DatePicker::widget([
					'model' => $searchModel,
					'attribute' => 'stopActivity',
					'type' => DatePicker::TYPE_INPUT,
				]),
				'attribute' => 'stopActivity',
				'format' => ['date', 'php:d.m.Y'],
				'headerOptions' => ['width' => '240'],
			],

			

			[
				'header'=>'Действия', 
				'class' => 'yii\grid\ActionColumn',
				'headerOptions' => ['width' => '80'],
			],
		],
	]); ?>
</div>
