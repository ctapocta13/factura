<?php
use yii\helpers\Html;
use yii\grid\GridView;
/* @var $this yii\web\View */

$this->title = 'Просмотр таблиц';
$this->params['breadcrumbs'][] = ['label'=>'Управление магазином', 'url' => ['/shop/admin']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-view">
	<h2><?=$this->title?></h2>
	<?php if (isset($errors)) print_r($errors);?>
	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		/*'filterModel' => $searchModel,*/
		'columns' => [
			/*[
				'filter' => DatePicker::widget([
					'model' => $searchModel,
					'attribute' => 'date_from',
					'attribute2' => 'date_to',
					'type' => DatePicker::TYPE_RANGE,
					'separator' => '-',
					'pluginOptions' => ['format' =>'yyyy-mm-dd']
				]),
				'attribute' => 'created_at',
				'format' => 'datetime',
			],*/
			/*
			[
				'class' => LinkColumn::className(),
				'attribute' => 'username',
			],*/
			/*
			'email:email',
			*/
			/*
			 [
				'class' =>SetColumn::className(),
				'filter' => User::getStatusesArray(),
				'attribute' => 'status',
				'name' => 'statusName',
				'cssCLasses' => [
					User::STATUS_ACTIVE => 'success',
					User::STATUS_WAIT => 'warning',
					User::STATUS_BLOCKED => 'default',
				],
			],*/
			/*
			[
			'class' => RoleColumn::className(),
			'filter' => ArrayHelper::map(Yii::$app->authManager->getRoles(), 'name', 'description'),
			'attribute' => 'role',
		],*/

			/*['class' => ActionColumn::className()],*/
		],
	]); ?>
</div>
