<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
use yii\helpers\ArrayHelper;
use app\rbac\Rbac as AdminRbac;

$this->title = 'Управление магазином';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shop-admin-index">
	<h2><?=$this->title?></h2>
	<?php if (\Yii::$app->user->can(AdminRbac::PERMISSION_USER_EDIT)):?>
		<h3>Управление пользователями</h3>
		<?= Html::a('Пользователи', ['/admin/users'], ['class' => 'btn btn-success']) ?>
	<?php endif?>
	<h3>Отчеты</h3>
	<?= Html::a('Заказы', ['/shop/admin'], ['class' => 'btn btn-success disabled']) ?>
	<?= Html::a('Выгрузка каталога', ['/shop/admin/catalog'], ['class' => 'btn btn-success']) ?>

	<h3>Управление каталогом</h3>
	<?= Html::a('Загрузить файл', ['upload'], ['class' => 'btn btn-success']) ?>
	<?= Html::a('Скачать таблицы', ['/shop/admin'], ['class' => 'btn btn-success disabled']) ?>
	<br/><br/>
	<?= Html::a('Единицы измерения', ['/shop/admin/units/index'], ['class' => 'btn btn-success']) ?>
	<?= Html::a('Категории', ['/shop/admin/categories/index'], ['class' => 'btn btn-success']) ?>
	<?= Html::a('Производители', ['/shop/admin/manufacturers/index'], ['class' => 'btn btn-success']) ?>
	<?= Html::a('Брэнды', ['/shop/admin/brands/index'], ['class' => 'btn btn-success']) ?>
	<?= Html::a('Товары', ['/shop/admin/products/index'], ['class' => 'btn btn-success']) ?>
	
	<h3>Управление контентом</h3>
	<?= Html::a('Слайдеры', ['/shop/admin/slider/index'], ['class' => 'btn btn-success']) ?>
	<?= Html::a('Страницы', ['/shop/admin/pages/index'], ['class' => 'btn btn-success']) ?>

</div>
