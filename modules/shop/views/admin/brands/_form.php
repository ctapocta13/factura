<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\shop\models\Brands */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="brands-form">

	<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'brandsSlug')->textInput(['disabled' => $model->isNewRecord ? false : true]) ?>

	<?= $form->field($model, 'brandsFullName')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'brandsShortName')->textInput(['maxlength' => true]) ?>

	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
