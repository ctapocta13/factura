<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\shop\models\Brands */

$this->title = 'Редактирование брэнда: ' . ' ' . $model->brandsSlug;
$this->params['breadcrumbs'][] = ['label' => 'Управление магазином', 'url' => ['/shop/admin']];
$this->params['breadcrumbs'][] = ['label' => 'Брэнды', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->brandsSlug, 'url' => ['view', 'id' => $model->brandsSlug]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="brands-update">

	<h2><?= Html::encode($this->title) ?></h2>

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
