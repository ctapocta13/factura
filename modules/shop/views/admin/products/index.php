<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\shop\models\ProductsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Товары';
$this->params['breadcrumbs'][] = ['label'=>'Управление магазином', 'url' => ['/shop/admin']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-index">

	<h2><?= Html::encode($this->title) ?></h2>
	<?php echo $this->render('_search', ['model' => $searchModel]); ?>
	<p>
		<?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
	</p>
	<?= ListView::widget([
		'dataProvider' => $dataProvider,
		'itemOptions' => ['class' => 'item'],
		'itemView' => function ($model, $key, $index, $widget) {
			return Html::a(Html::encode($model->getShortName()), ['view', 'id' => $model->productsSlug]);
		},
	]) ?>

</div>
