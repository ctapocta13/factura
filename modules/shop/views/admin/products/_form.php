<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\shop\models\Units;
use app\modules\shop\models\Brands;
use app\modules\shop\models\Categories;
use app\modules\shop\models\Manufacturers;
/* @var $this yii\web\View */
/* @var $model app\modules\shop\models\Products */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="products-form">
	<div class="alert alert-danger">Все это будет переделываться</div>
	<?php $form = ActiveForm::begin(); ?>
	<?php
		$categories=Categories::getFullList();
		$class="col-lg-3 col-md-3 col-sm-3 col-xs-3";
		$class2="col-lg-2 col-md-2 col-sm-2 col-xs-2";
	?>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?= $form->field($model, 'productsSlug')
				->textInput(['disabled' => $model->isNewRecord ? false : true]) ?>
		</div>
	</div>
	<div class="row">
		<div class="<?=$class?>">
			<?= $form->field($model, 'productsCat0')->dropDownList($categories)?>
		</div>
		<div class="<?=$class?>">
			<?= $form->field($model, 'productsCat1')->dropDownList($categories)?>
		</div>
		<div class="<?=$class?>">
			<?= $form->field($model, 'productsCat2')->dropDownList($categories)?>
		</div>
		<div class="<?=$class?>">
			<?= $form->field($model, 'productsCat3')->dropDownList($categories)?>
		</div>
	</div>
	<div class="row">
		<div class="<?=$class2?>">
			<?= $form->field($model, 'productsType')->dropDownList($categories)?>
		</div>
		<div class="<?=$class2?>">
			<?= $form->field($model, 'productsUnitSlug')->dropDownList(Units::getFullList()) ?>
		</div>
		<div class="<?=$class2?>">
			<?= $form->field($model, 'productsUnitValue')->textInput(['maxlength' => true]) ?>
		</div>
		<div class="<?=$class2?>">
			<?= $form->field($model, 'productsBrandSlug')->dropDownList(Brands::getFullList())?>
		</div>
		<div class="<?=$class2?>">
			<?= $form-
				>field($model, 'productsManufacturerSlug')
				->dropDownList(Manufacturers::getFullList())?>
		</div>
		<div class="<?=$class2?>">
			<?= $form->field($model, 'productsName')->textInput(['maxlength' => true]) ?>
		</div>
	</div>
	<div class="row">
		<div class="<?=$class2?>">
			<?= $form->field($model, 'productsStock')->textInput() ?>
		</div>
		<div class="<?=$class2?>">
			<?= $form->field($model, 'productsPrice1')->textInput() ?>
		</div>
		<div class="<?=$class2?>">
			<?= $form->field($model, 'productsPrice2')->textInput() ?>
		</div>
		<div class="<?=$class2?>">
			<?= $form->field($model, 'productsPrice3')->textInput() ?>
		</div>
		<div class="<?=$class2?>">
			<?= $form->field($model, 'productsPrice4')->textInput() ?>
		</div>
		<div class="<?=$class2?>">
			<?= $form->field($model, 'productsPrice5')->textInput() ?>
		</div>
	</div>
	<div class="row">
		<div class="<?=$class2?>">
			<?= $form->field($model, 'productsHeight')->textInput() ?>
		</div>
		<div class="<?=$class2?>">
			<?= $form->field($model, 'productsWidth')->textInput() ?>
		</div>
		<div class="<?=$class2?>">
			<?= $form->field($model, 'productsLong')->textInput() ?>
		</div>
		<div class="<?=$class2?>">
			<?= $form->field($model, 'productsDiameter')->textInput() ?>
		</div>
		<div class="<?=$class2?>">
			<?= $form->field($model, 'productsColor')->textInput(['maxlength' => true]) ?>
		</div>
		<div class="<?=$class2?>">
			<?= $form->field($model, 'productsMaterial')->textInput(['maxlength' => true]) ?>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?= $form->field($model, 'productsDescriptionFull')->textarea(['rows' => 6]) ?>
		</div>
	</div>
	<div class="row">
		<div class="<?=$class?>">
			<?= $form->field($model, 'productsIsAction')->dropDownList([true=>'Да', false=>'Нет'])?>
		</div>
		<div class="<?=$class?>">
			<?= $form->field($model, 'productsIsActive')->dropDownList([true=>'Да', false=>'Нет'])?>
		</div>
		<div class="<?=$class?>">
		</div>
		<div class="<?=$class?>">
		</div>
	</div>
	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? 
			'Создать' : 
			'Сохранить', ['class' => $model->isNewRecord ?
				'btn btn-success' :
				'btn btn-primary']) ?>
	</div>
	<?php ActiveForm::end(); ?>
</div>
