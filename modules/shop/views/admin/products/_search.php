<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\shop\models\ProductsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="products-search">

	<?php $form = ActiveForm::begin([
			'action' => ['index'],
			'method' => 'get',
		]);
		$class="col-lg-2 col-md-2 col-sm-2 col-xs-2";
	?>
	<div class="row">
		<div class="<?=$class?>">
			<?= $form->field($model, 'productsSlug') ?>
		</div>
		<div class="<?=$class?>">
			<?= $form->field($model, 'productsCat0')->dropDownList($model->getCat0List()) ?>
		</div>
		<div class="<?=$class?>">
			<?= $form->field($model, 'productsCat1')->dropDownList($model->getCat1List()) ?>
		</div>
		<div class="<?=$class?>">
			<?= $form->field($model, 'productsCat2')->dropDownList($model->getCat2List()) ?>
		</div>
		<div class="<?=$class?>">
			<?= $form->field($model, 'productsCat3')->dropDownList($model->getCat3List()) ?>
		</div>
		<div class="<?=$class?>">
			<?= $form->field($model, 'productsType')->dropDownList($model->getTypeList()) ?>
		</div>
	</div>
	<div class="row">
		<div class="<?=$class?>">
			<?= $form->field($model, 'productsName') ?>
		</div>
		<div class="<?=$class?>">
			<?= $form->field($model, 'productsUnitValue') ?>
		</div>
		<div class="<?=$class?>">
			<?= $form->field($model, 'productsStock') ?>
		</div>
		<div class="<?=$class?>">
			<?= $form->field($model, 'productsColor') ?>
		</div>
		<div class="<?=$class?>">
			<?= $form->field($model, 'productsMaterial') ?>
		</div>
		<div class="<?=$class?>">
			<?= $form->field($model, 'productsDescriptionFull') ?>
		</div>
	</div>
	<div class="row">
		<div class="<?=$class?>">
			<?= $form->field($model, 'productsManufacturerSlug')->dropDownList($model->getManufacturersList()) ?>
		</div>
		<div class="<?=$class?>">
			<?= $form->field($model, 'productsBrandSlug')->dropDownList($model->getBrandsList()) ?>
		</div>
		<div class="<?=$class?>">
			<?= $form->field($model, 'productsIsAction')->dropDownList([''=>'Все',true=>'Да', false=>'Нет']) ?>
		</div>
		<div class="<?=$class?>">
			<?= $form->field($model, 'productsIsActive')->dropDownList([''=>'Все',true=>'Да', false=>'Нет']) ?>
		</div>
			<?php // echo $form->field($model, 'productsUnitSlug') ?>
			<?php // echo $form->field($model, 'productsHeight') ?>
			<?php // echo $form->field($model, 'productsWidth') ?>
			<?php // echo $form->field($model, 'productsLong') ?>
			<?php // echo $form->field($model, 'productsSize') ?>
			<?php // echo $form->field($model, 'productsDiameter') ?>
	</div>

	<div class="form-group">
		<?= Html::submitButton('Найти', ['class' => 'btn btn-primary']) ?>
		<?= Html::resetButton('Отмена', ['class' => 'btn btn-default']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
