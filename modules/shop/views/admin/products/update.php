<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\shop\models\Products */

$this->title = 'Редактирование товара: ' . ' ' . $model->getShortName();
$this->params['breadcrumbs'][] = ['label'=>'Управление магазином', 'url' => ['/shop/admin']];
$this->params['breadcrumbs'][] = ['label' => 'Товары', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->getShortName(), 'url' => ['view', 'id' => $model->productsSlug]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="products-update">

	<h2><?= Html::encode($this->title) ?></h2>

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
