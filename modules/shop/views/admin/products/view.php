<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\shop\models\Products */

$this->title = $model->getShortName();
$this->params['breadcrumbs'][] = ['label'=>'Управление магазином', 'url' => ['/shop/admin']];
$this->params['breadcrumbs'][] = ['label' => 'Товары', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-view">

	<h2><?= Html::encode($this->title) ?></h2>

	<p>
		<?= Html::a('Редактировать', ['update', 'id' => $model->productsSlug], ['class' => 'btn btn-primary']) ?>
	</p>

	<?= DetailView::widget([
		'model' => $model,
		'attributes' => [
			'productsSlug',
			'productsPhoto0',
			'productsPhoto1',
			'productsPhoto2',
			'productsPhoto3',
			'productsCat0',
			'productsCat1',
			'productsCat2',
			'productsCat3',
			'productsType',
			'productsName',
			'productsUnitSlug',
			'productsUnitValue',
			'productsBrandSlug',
			'productsManufacturerSlug',
			'productsStock',
			'productsPrice1',
			'productsPrice2',
			'productsPrice3',
			'productsPrice4',
			'productsPrice5',
			'productsDescriptionFull:ntext',
			'productsHeight',
			'productsWidth',
			'productsLong',
			'productsSize',
			'productsDiameter',
			'productsColor',
			'productsMaterial',
			'productsIsAction',
			'productsIsActive',
		],
	]) ?>

</div>
