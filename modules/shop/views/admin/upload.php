<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
/* @var $this yii\web\View */

$this->title = 'Загрузка файла';
$this->params['breadcrumbs'][] = ['label'=>'Управление магазином', 'url' => ['/shop/admin']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shop-admin-upload">
	<h2><?=$this->title?></h2>
	<div class="alert alert-danger" role="alert">
		<strong>Внимание!</strong> Изменения в базу данных производятся сразу после загрузки и необратимы. <strong>Будьте внимательны!</strong></div>
	<div class="alert alert-info" role="alert">
		<p>Имя таблицы куда будет произведен импорт берется из названия файла до дефиса (например <strong>products-price.csv</strong> и <strong>products.csv</strong> оба будут загружены в таблицу <strong>products</strong>)</p>
		<p><strong>Формат файла</strong>:</p>
		<ul>
			<li><strong>CSV</strong></li>
			<li>Кодировка <strong>UTF-8</strong></li>
			<li>Разделитель полей : <strong>|</strong></li>
			<li>Все остальные галочки должны <strong>сняты</strong></li>
			<li>В первой строке - название обновляемых полей</li>
			<li>При обновлении записей наличие всех полей необязательно. Достаточно первого поля и всех обновляемых</li>
		</ul>
		<p>Всего доступны для загрузки <strong>5</strong> таблиц. Все остальные файлы будет проигнорированы</p>
		<?php
			Modal::begin([
				'id' => 'tables-info',
				'size' => "modal-lg",
				'header' => '<h3>Справка по таблицам</h3>',
				'toggleButton' => ['label' => 'Справка по таблицам', 'class'=>'btn btn-info'],
			]);
		?>
		<table class="table table-striped table-bordered">
			<tr>
				<th>Имя таблицы/файла</th><th>Роль на сайте</th><th>Комментарий</th>
			</tr>
			<tr>
				<th>products</th><td>Все свойства продуктов</td><td>Основная таблица. Связывается с остальными по ключевым полям</td>
				
			</tr>
			<tr>
				<th>brands</th><td>Все свойства брэндов</td><td>Ключевое поле в продуктах <strong>productsBrandSlug</strong></td>
			</tr>
			<tr>
				<th>categories</th>
				<td>Тип товара и название категорий в каталоге</td><td>Ключевые поля в продуктах:
					<ul>
						<li><strong>productsCat0</strong></li>
						<li><strong>productsCat1</strong></li>
						<li><strong>productsCat2</strong></li>
						<li><strong>productsCat3</strong></li>
						<li><strong>productsType</strong></li>
					</ul>
				</td>
			</tr>
			<tr>
				<th>units</th><td>Единицы измерения</td><td>Ключевое поле в продуктах <strong>productsUnitSlug</strong></td>
			</tr>
			<tr>
				<th>manufacturers</th><td>Производители</td><td>Ключевое поле в продуктах <strong>productsManufacturerSlug</strong></td>
			</tr>
		</table>
		<?php
			Modal::end();
		?>
	</div>
	<div class="row">
			<div class="col-lg-12">

				<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

					<?= $form->field($model, 'fileToUpload')->fileInput() ?>


						<?= Html::submitButton('Загрузить', ['class' => 'btn btn-success']) ?>

				<?php ActiveForm::end(); ?>

			</div>
		</div>
</div>
