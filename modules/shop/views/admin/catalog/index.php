<?php
use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this yii\web\View */
$this->title = 'Центр загрузок';
$this->params['breadcrumbs'][] = ['label' => 'Управление магазином', 'url' => ['/shop/admin']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shop-admin-catalog-index">
	<h2><?=$this->title?></h2>
	<?php
		$filename= file_exists('files/catalog.pdf') ? 'files/catalog.pdf' : false;
	?>
	<p>Дата последнего обновления каталога : <strong><?=!$filename ? 'Никогда' : date('d.m.Y', filemtime($filename))?></strong></p>
	
	<?= Html::a('Обновить', ['update'], ['class' => 'btn btn-success']) ?>
	<?= !$filename ? '' : Html::a('Скачать', Url::to('@web/'.$filename), ['class' => 'btn btn-success', 'target'=>'blank']) ?>
</div>
