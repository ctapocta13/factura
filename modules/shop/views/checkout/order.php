<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */

$this->title = 'Оформление заказа';
$this->params['breadcrumbs'][] = ['label'=>'Каталог', 'url' => ['/shop/catalog']];
$this->params['breadcrumbs'][] = ['label'=>'Корзина', 'url' => ['/shop/checkout']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shop-checkout-order">
	<h2><?=$this->title?></h2>

	<div class="row">
			<div class="col-lg-12">

				<?php $form = ActiveForm::begin(['id' => 'shop-checkout-order-form']); ?>

					<?= $form->field($model, 'deliveryDate')->widget(
					DatePicker::classname(),[
						'type' => 3 /*TYPE_COMPONENT_APPEND*/,
						'options' => ['placeholder' => 'Выберите желаемую дату доставки'],
						'pluginOptions' => [
							'format' => 'dd.mm.yyyy',
							'autoclose' => true,
							'todayHighlight' => true,
							'startDate' => date('d-m-Y', strtotime('+2 days')),
							'endDate' => date('d-m-Y', strtotime('+23 days')),

					]])?>

					<?= $form->field($model, 'name') ?>
					
					<?= $form->field($model, 'phone') ?>

					<?= $form->field($model, 'email') ?>

					<?= $form->field($model, 'address') ?>

					<?= $form->field($model, 'comment')->textArea(['rows' => 6]) ?>

					<div class="form-group">
						<?= Html::a('Вернуться', ['checkout'], ['class' => 'btn btn-success pull-left']) ?>
						<?= Html::submitButton('Завершить', ['class' => 'btn btn-success pull-right', 'name' => 'thanks-button']) ?>
					</div>

				<?php ActiveForm::end(); ?>

			</div>
		</div>
</div>
