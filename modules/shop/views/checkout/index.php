<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

use app\modules\shop\models\Products;

/* @var $this yii\web\View */

$this->title = 'Корзина';
$this->params['breadcrumbs'][] = ['label'=>'Каталог', 'url' => ['/shop/catalog']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shop-checkout-index">
	<h2>Ваша корзина</h2>
	<div class="row">
		<h4 class="col-lg-6 col-md-6 col-sm-5 col-xs-5"></h4>
		<h4 class="col-lg-2 col-md-2 col-sm-2 col-xs-2 text-center">Цена</h4>
		<h4 class="col-lg-2 col-md-2 col-sm-3 col-xs-3 text-center">Количество</h4>
		<h4 class="col-lg-2 col-md-2 col-sm-2 col-xs-2 text-center">Сумма</h4>
	</div>
	<?php Pjax::begin(['enablePushState' => false, 'id'=>'pjax'])?>
		<?php if ($model->products!==false);
			$totalCount=$totalSum=0;
			foreach($model->products as $resultRow):?>
				<div class="row">
					<?php $product=Products::findOne($resultRow['id'])?>
					<div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">
						<div class='product-image-wrapper product-image-wrapper-icon img-responsive'>
							<?=Html::img($product->getPhotoLink(Products::PHOTO_ICON),[
								'class'=>'product-image',
								'alt'=>$product->getAlt(),
							])?>
						</div>
					</div>
					<div class="col-lg-5 col-md-5 col-sm-3 col-xs-3">
						<?=Html::a(
							$product->getShortName(),
							['/shop/product','id'=>$product->productsSlug]
						)?>
					</div>
					<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 text-center">
						<span><?=$product->getPrice().' руб.'?></span>
					</div>
					<div class="col-lg-2 col-md-2 col-sm-3 col-xs-3 text-center">
						<?= Html::a('-', ['checkout-pjax', 'id'=>$product->productsSlug, 'delta'=>-1], ['class' => 'btn btn-success']) ?>
						<span><?=$resultRow['quant'].' '.$product->unit->unitsShortName?></span>
						<?= Html::a('+', ['checkout-pjax', 'id'=>$product->productsSlug, 'delta'=>1], ['class' => 'btn btn-success']) ?>
					</div>
					<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 text-center">
						<span><?=$product->getPrice()*$resultRow['quant'].' руб.'?></span>
					</div>
				</div>
				<?php
					$totalSum+=$product->getPrice()*$resultRow['quant'];
					$totalCount+=$resultRow['quant'];
				?>
				<hr/>
			<?php endforeach?>
			<div class="row">
				<h4 class="col-lg-8 col-md-8 col-sm-7 col-xs-7"></h4>
				<h4 class="col-lg-2 col-md-2 col-sm-3 col-xs-3 text-center"><?=$totalCount?></h4>
				<h4 class="col-lg-2 col-md-2 col-sm-2 col-xs-2 text-center"><?=$totalSum?> руб.</h4>
			</div>
			<br/>
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<?= Html::a('Продолжить покупки', ['/shop/catalog'], ['class' => 'btn btn-success pull-left', 'data-pjax'=>'0']) ?>
					<?= Html::a('Очистить корзину', ['/shop/default/clean-basket'], ['class' => 'btn btn-success pull-left', 'data-pjax'=>'0']) ?>
					<?= Html::a('Оформить заказ', ['order'], ['class' => 'btn btn-success pull-right', 'data-pjax'=>'0']) ?>
				</div>
			</div>
	<?php Pjax::end()?>
</div>
