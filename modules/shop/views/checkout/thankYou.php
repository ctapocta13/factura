<?php

use yii\helpers\Html;
use yii\helpers\Url;
//use yii\bootstrap\Collapse;
use yii\widgets\ListView;
use app\components\widgets\BasketWidget;
use yii\widgets\Pjax;

/* @var $this yii\web\View */

$this->title = 'Спасибо!';
$this->params['breadcrumbs'][] = ['label'=>'Каталог', 'url' => ['/shop/catalog']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shop-checkout-thankYou">
	<h2><?=$this->title?></h2>
	<p>Ваш заказ № <strong><?=$model->order->id?></strong> успешно принят и выполняется</p>
</div>
