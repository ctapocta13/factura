<?php

use yii\helpers\Html;
use yii\helpers\Url;

use app\components\Basket;
use app\modules\shop\models\Products;
/* @var $this yii\web\View */

$this->title = $product->getTitle();
$catalog='/shop/catalog';
//$this->params['breadcrumbs'][] = ['label'=>'Каталог', 'url' => [$catalog, 'id' =>'']];
if ($product->productsCat0) {
	$this->params['breadcrumbs'][] = ['label' => $product->cat0->categoriesShortName, 'url' => [$catalog, 'id' => $product->productsCat0]];
	if ($product->productsCat1) {
		$this->params['breadcrumbs'][] = ['label' => $product->cat1->categoriesShortName, 'url' => [$catalog, 'id' => $product->productsCat0.'/'.$product->productsCat1]];
		if ($product->productsCat2) {
			$this->params['breadcrumbs'][] = ['label' => $product->cat2->categoriesShortName, 'url' => [$catalog, 'id' => $product->productsCat0.'/'.$product->productsCat1.'/'.$product->productsCat2]];
			if ($product->productsCat3) $this->params['breadcrumbs'][] = ['label' => $product->cat3->categoriesShortName, 'url' => [$catalog, 'id' => $product->productsCat0.'/'.$product->productsCat1.'/'.$product->productsCat2.'/'.$product->productsCat3]];;
		}
	}/*А вообще продумать как это сделать в 1 цикле*/
}
if ($product->productsIsSQU==Products::SQU_SUB)
	$this->params['breadcrumbs'][] = [
		'label'=>$product->parentProduct->getTitle(), 
		'url' => ['/shop/product', 'id' =>$product->productsSQUParentId]
	];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shop-product-index row">
	<div class="col-lg-4 col-md-5 col-sm-6 col-xs-6">
		<?php for ($i=0; $i<4; $i++) :?>
			<?php if ($product->attributes['productsPhoto'.$i]=='') break; ?>
			<div id="photo<?=$i?>" 
				class='product-image-wrapper product-image-wrapper-full img-responsive<?=!$i ? '' : ' hidden'?>'>
				<?=Html::img($product->getPhotoLink(Products::PHOTO_MAIN, $i),[
					'class'=>'product-image',
					'alt'=>$product->getAlt(),
				])?>
			</div>
		<?php endfor ?> 
		<?php if ($i>1) :?> <div class="row products-controlbar">
			<?php for ($j=0; $j<$i; $j++): ?>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
					<div class="photo-button" data-photoid="photo<?=$j?>">
						<div class='product-image-wrapper product-image-wrapper-icon img-responsive'>
							<?=Html::img($product->getPhotoLink(Products::PHOTO_ICON, $j),[
								'class'=>'product-image',
								'alt'=>$product->getAlt(),
							])?>
						</div>
					</div>
				</div>
			<?php endfor ?> </div>
		<?php endif ?>
		<?php if ($product->productsIsAction):?>
			<div class="product-action btn btn-danger">Акция!</div>
		<?php endif?>
		<?php if ($product->productsActionDiscount):?>
			<div class="product-action-discount btn btn-info">-<?=$product->productsActionDiscount?>%</div>
		<?php endif?>
	</div>
	<div class="col-lg-8 col-md-7 col-sm-6 col-xs-6">
		<h2><?=$product->getFullName()?></h2>
		<?php $labels=$product->attributeLabels();?>
		<?php if ($product->productsHeight):?>
			<p><?=$labels['productsHeight']?>: <?=$product->productsHeight?></p>
		<?php endif?>
		<?php if ($product->productsWidth):?>
			<p><?=$labels['productsWidth']?>: <?=$product->productsWidth?></p>
		<?php endif?>
		<?php if ($product->productsLong):?>
			<p><?=$labels['productsLong']?>: <?=$product->productsLong?></p>
		<?php endif?>
		<?php if($product->productsIsSQU==Products::SQU_MAIN):?>
			<div class="product-squ-filter">
				<span class="product-squ-filter-name"><?=$product->productsSQUFilterName?>:</span>
				<?php foreach($product->subProducts as $subProduct):?>
					<span class="product-squ-filter-item" data-productid="<?=$subProduct->productsSlug?>" data-price="<?=$subProduct->getPrice()?>">
						<?=$subProduct->productsSQUFilterName?>
					</span>
				<?php endforeach?>
			</div>
		<?php endif?>
		<?php if ($product->productsSize):?>
			<p><?=$labels['productsSize']?>:<?=$product->productsSize?></p>
		<?php endif?>
		<?php if ($product->productsDiameter):?>
			<p><?=$labels['productsDiameter']?>: <?=$product->productsDiameter?></p>
		<?php endif?>
		<?php if ($product->productsColor):?>
			<p><?=$labels['productsColor']?>: <?=$product->productsColor?></p>
		<?php endif?>
		<?php if ($product->productsMaterial):?>
			<p><?=$labels['productsMaterial']?>: <?=$product->productsMaterial?></p>
		<?php endif?>
		<?php if ($product->productsManufacturerSlug):?>
			<p><?=$labels['productsManufacturerSlug'].': '.$product->manufacturer->manufacturersFullName?></p>
		<?php endif?>
		<p><?=$labels['productsUnitValue']?>: <?=$product->productsUnitValue?> <?=$product->unit->unitsShortName?></p>
		<?php if ($product->getPrice()>0 && $product->productsStock>0 && $product->productsIsActive>0):?>
			<div class="products-detail-price">
				<span class="products-details-price-label">Цена: </span>
				<span class="products-details-price-value"><?=$product->getPrice()?></span>
				<span class="products-details-price-label"> руб.</span>
			</div>
			<?php
				$basket=new Basket;
				$key=$basket->inBasket($product->productsSlug);
			?>
				<div class="in-basket <?=$key===false ? 'hide' :''?>">
					<span class="in-basket-count"><?=$key===false ? 0 : $basket->products[$key]['quant']?></span>
					<span> в корзине</span>
				</div>
			<a class="btn btn-success intoBasket<?=($product->productsIsSQU!=Products::SQU_MAIN) ? '' : ' disabled'?>" data-productid="<?=$product->productsSlug?>" >В корзину</a>
		<?php else:?>
			<p class="btn btn-danger">Товар временно отсутстует в продаже</p>
		<?php endif?>

		<hr/>
		<?php if ($product->productsDescriptionFull):?>
			<div class="product-description"><?=$product->productsDescriptionFull?></div>
		<?php else: ?>
			<?php if($product->productsIsSQU==Products::SQU_SUB) if ($product->parentProduct->productsDescriptionFull):?>
				<div class="product-description"><?=$product->parentProduct->productsDescriptionFull?></div>
			<?php endif ?>
		<?php endif?>
		<?php if ($product->productsPDFLink):?>
			<?= Html::a('Посмотреть презентацию', Url::to('@web/'.$product->productsPDFLink), ['target'=>'_blank']) ?>
		<?php endif?>
	</div>
</div>
