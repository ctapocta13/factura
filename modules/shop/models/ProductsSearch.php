<?php

namespace app\modules\shop\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
//use app\modules\shop\models\Products;
use yii\helpers\ArrayHelper;

/**
 * ProductsSearch represents the model behind the search form about `app\modules\shop\models\Products`.
 */
class ProductsSearch extends Products
{
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['productsSlug', 'productsCat0', 'productsCat1', 'productsCat2', 'productsCat3', 'productsType', 'productsName', 'productsBrandSlug', 'productsManufacturerSlug', 'productsColor', 'productsMaterial'], 'safe'],
			[['productsStock', 'productsIsAction', 'productsIsActive'], 'integer'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$query = Products::find();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		$query->andFilterWhere([
			'productsStock' => $this->productsStock,
			'productsIsAction' => $this->productsIsAction,
			'productsIsActive' => $this->productsIsActive,
		]);

		$query->andFilterWhere(['like', 'productsSlug', $this->productsSlug])
			->andFilterWhere(['like', 'productsCat0', $this->productsCat0])
			->andFilterWhere(['like', 'productsCat1', $this->productsCat1])
			->andFilterWhere(['like', 'productsCat2', $this->productsCat2])
			->andFilterWhere(['like', 'productsCat3', $this->productsCat3])
			->andFilterWhere(['like', 'productsType', $this->productsType])
			->andFilterWhere(['like', 'productsName', $this->productsName])
			->andFilterWhere(['like', 'productsBrandSlug', $this->productsBrandSlug])
			->andFilterWhere(['like', 'productsManufacturerSlug', $this->productsManufacturerSlug])
			->andFilterWhere(['like', 'productsDescriptionFull', $this->productsDescriptionFull])
			->andFilterWhere(['like', 'productsColor', $this->productsColor])
			->andFilterWhere(['like', 'productsMaterial', $this->productsMaterial]);

		return $dataProvider;
	}
	
	public function getCat0List(){
		$bar=ArrayHelper::getColumn(Products::findBySql('SELECT distinct productsCat0 FROM products order by productsCat0')->all(), 'productsCat0');
		$foo=ArrayHelper::getColumn(Categories::find()->where(['categoriesSlug'=>$bar])->orderBy('categoriesSlug')->all(), 'categoriesFullName');
		$foobar=array_combine($bar,$foo);
		$foobar=ArrayHelper::merge([''=>'Все'], $foobar);
		return $foobar;
	}
	public function getCat1List(){
		$bar=ArrayHelper::getColumn(Products::findBySql('SELECT distinct productsCat1 FROM products where productsCat1!="" order by productsCat1')->all(), 'productsCat1');
		$foo=ArrayHelper::getColumn(Categories::find()->where(['categoriesSlug'=>$bar])->orderBy('categoriesSlug')->all(), 'categoriesFullName');
		$foobar=array_combine($bar,$foo);
		$foobar=ArrayHelper::merge([''=>'Все'], $foobar);
		return $foobar;
	}
	public function getCat2List(){
		$bar=ArrayHelper::getColumn(Products::findBySql('SELECT distinct productsCat2 FROM products where productsCat2!="" order by productsCat2')->all(), 'productsCat2');
		$foo=ArrayHelper::getColumn(Categories::find()->where(['categoriesSlug'=>$bar])->orderBy('categoriesSlug')->all(), 'categoriesFullName');
		$foobar=array_combine($bar,$foo);
		$foobar=ArrayHelper::merge([''=>'Все'], $foobar);
		return $foobar;
	}
	public function getCat3List(){
		$bar=ArrayHelper::getColumn(Products::findBySql('SELECT distinct productsCat3 FROM products where productsCat3!="" order by productsCat3')->all(), 'productsCat3');
		$foo=ArrayHelper::getColumn(Categories::find()->where(['categoriesSlug'=>$bar])->orderBy('categoriesSlug')->all(), 'categoriesFullName');
		$foobar=array_combine($bar,$foo);
		$foobar=ArrayHelper::merge([''=>'Все'], $foobar);
		return $foobar;
	}
	public function getTypeList(){
		$bar=ArrayHelper::getColumn(Products::findBySql('SELECT distinct productsType FROM products order by productsType')->all(), 'productsType');
		$foo=ArrayHelper::getColumn(Categories::find()->where(['categoriesSlug'=>$bar])->orderBy('categoriesSlug')->all(), 'categoriesFullName');
		$foobar=array_combine($bar,$foo);
		$foobar=ArrayHelper::merge([''=>'Все'], $foobar);
		return $foobar;
	}
	public function getManufacturersList(){
		$bar=ArrayHelper::getColumn(Products::findBySql('SELECT distinct productsManufacturerSlug FROM products order by productsManufacturerSlug')->all(), 'productsManufacturerSlug');
		$foo=ArrayHelper::getColumn(Manufacturers::find()->where(['manufacturersSlug'=>$bar])->orderBy('manufacturersSlug')->all(), 'manufacturersFullName');
		$foobar=array_combine($bar,$foo);
		$foobar=ArrayHelper::merge([''=>'Все'], $foobar);
		return $foobar;
	}
	public function getBrandsList(){
		$bar=ArrayHelper::getColumn(Products::findBySql('SELECT distinct productsBrandSlug FROM products order by productsBrandSlug')->all(), 'productsBrandSlug');
		$foo=ArrayHelper::getColumn(Brands::find()->where(['brandsSlug'=>$bar])->orderBy('brandsSlug')->all(), 'brandsFullName');
		$foobar=array_combine($bar,$foo);
		$foobar=ArrayHelper::merge([''=>'Все'], $foobar);
		return $foobar;
	}
}
