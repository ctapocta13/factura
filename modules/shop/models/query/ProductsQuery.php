<?php
namespace app\modules\shop\models\query;
 
use app\modules\shop\models\Products;
use yii\db\ActiveQuery;
use Yii;
 
class ProductsQuery extends ActiveQuery
{
	public function toShow()
	{
		return $this
			->where(['productsIsActive'=>true])
			->andWhere(['<', 'productsIsSQU', Products::SQU_SUB])
			->andWhere(['>', 'productsPrice', 0])
			->andWhere(['>', 'productsStock', 0]);
	}
	public function toPath($cats)
	{
		if ($cats=='') return $this;
		else{
			$query=$this;
			for($i=0;$i<count($cats);$i++) $query->andWhere(['productsCat'.$i=>$cats[$i]]);
			return $query;
		}
	}
}
?>
