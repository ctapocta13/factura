<?php

namespace app\modules\shop\models;

use Yii;

/**
 * This is the model class for table "ordersH".
 *
 * @property integer $id
 * @property integer $userId
 * @property integer $date
 * @property integer $isPayed
 * @property integer $isDelivered
 * @property integer $isCancelled
 */
class OrdersH extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'ordersH';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['customerName', 'customerPhone', 'deliveryAddress'], 'required'],
			[['userId', 'date', 'isPayed', 'isDelivered', 'isCancelled', 'deliveryDate'], 'integer'],
			[['customerName', 'comment', 'customerPhone', 'deliveryAddress', 'email'], 'string', 'max' => 255],
			['email', 'email'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'userId' => 'ID Пользователя',
			'date' => 'Дата',
			'isPayed' => 'Оплачен',
			'isDelivered' => 'Доставлен',
			'isCancelled' => 'Отменен',
			'customerName' => 'Имя покупателя',
			'customerPhone' => 'Телефон покупателя',
			'deliveryAddress' => 'Адрес доставки',
			'deliveryDate' => 'Дата доставки',
		];
	}
	public function getDetails(){
		return $this->hasMany(OrdersD::className(), ['orderId'=>'id']);
	}
}
