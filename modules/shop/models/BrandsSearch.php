<?php

namespace app\modules\shop\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\shop\models\Brands;

/**
 * BrandsSearch represents the model behind the search form about `app\modules\shop\models\Brands`.
 */
class BrandsSearch extends Brands
{
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['brandsSlug', 'brandsFullName', 'brandsShortName'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$query = Brands::find();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		$query->andFilterWhere(['like', 'brandsSlug', $this->brandsSlug])
//			->andFilterWhere(['like', 'brandsPhoto', $this->brandsPhoto])
			->andFilterWhere(['like', 'brandsFullName', $this->brandsFullName])
			->andFilterWhere(['like', 'brandsShortName', $this->brandsShortName]);

		return $dataProvider;
	}
}
