<?php

namespace app\modules\shop\models;

use Yii;

/**
 * This is the model class for table "ordersD".
 *
 * @property integer $id
 * @property integer $orderId
 * @property string $productSlug
 * @property integer $quant
 * @property integer $price
 */
class OrdersD extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'ordersD';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['orderId', 'quant', 'price'], 'required'],
			[['orderId', 'quant', 'price'], 'integer'],
			[['productSlug'], 'string', 'max' => 255]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'orderId' => 'ID Заказа',
			'productSlug' => 'Код продукта',
			'quant' => 'Количество',
			'price' => 'Цена',
		];
	}
	public function getOrderH(){
		return $this->hasOne(OrdersH::className(), ['id'=>'orderId']);
	}
	public function getProduct(){
		return $this->hasOne(Products::className(), ['productsSlug'=>'productSlug']);
	}
}
