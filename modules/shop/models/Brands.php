<?php

namespace app\modules\shop\models;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * This is the model class for table "brands".
 *
 * @property string $brandsSlug
 * @property string $brandsPhoto
 * @property string $brandsFullName
 * @property string $brandsShortName
 */
class Brands extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'brands';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['brandsSlug', 'brandsFullName'], 'required'],
			[['brandsSlug', 'brandsPhoto', 'brandsFullName', 'brandsShortName'], 'string', 'max' => 255]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'brandsSlug' => 'Код',
			'brandsPhoto' => 'Фото',
			'brandsFullName' => 'Полное название',
			'brandsShortName' => 'Краткое название',
		];
	}
	public static function getFullList(){
		$records=Brands::find()->all();
		return array_combine(ArrayHelper::getColumn($records,'brandsSlug'),ArrayHelper::getColumn($records,'brandsFullName'));
	}
}
