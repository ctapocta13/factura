<?php

namespace app\modules\shop\models;
//use app\models\LoadFromCsvModel;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "units".
 *
 * @property string $unitsSlug
 * @property string $unitsFullName
 * @property string $unitsShortName
 */
class Units extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'units';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['unitsSlug', 'unitsFullName', 'unitsShortName'], 'required'],
			[['unitsSlug', 'unitsFullName', 'unitsShortName'], 'string', 'max' => 255]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'unitsSlug' => 'Код',
			'unitsFullName' => 'Полное название',
			'unitsShortName' => 'Краткое название',
		];
	}
	public static function getFullList(){
		$records=self::find()->all();
		return array_combine(ArrayHelper::getColumn($records,'unitsSlug'),ArrayHelper::getColumn($records,'unitsFullName'));
	}
}
