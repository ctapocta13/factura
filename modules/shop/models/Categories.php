<?php

namespace app\modules\shop\models;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * This is the model class for table "categories".
 *
 * @property string $categoriesSlug
 * @property string $categoriesFullName
 * @property string $categoriesShortName
 */
class Categories extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'categories';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['categoriesSlug', 'categoriesFullName', 'categoriesShortName'], 'required'],
			[['categoriesSlug', 'categoriesFullName', 'categoriesShortName'], 'string', 'max' => 255]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'categoriesSlug' => 'Код',
			'categoriesFullName' => 'Полное наименование',
			'categoriesShortName' => 'Краткое наименование',
		];
	}
	public static function getFullList(){
		$records=self::find()->all();
		$fullList=array_combine(ArrayHelper::getColumn($records,'categoriesSlug'),ArrayHelper::getColumn($records,'categoriesFullName'));
		$fullList['']='Нет';
		return $fullList;
	}
}
