<?php

namespace app\modules\shop\models\forms;
use yii\base\Model;

use app\modules\shop\models\Products;
use app\modules\shop\models\Brands;
use app\modules\shop\models\Units;
use app\modules\shop\models\Manufacturers;
use app\modules\shop\models\Categories;

use yii\helpers\ArrayHelper;
use \SplFileObject;

//use Yii;
class UploadForm extends Model{
	public $fileToUpload;
	public $table;
	public $errors;
	public $filename;
	public $counters;
	public $columnCount=0;
	protected $fileObject;
	
	public function loadFromCsv()
	{
		$this->counters['rowsUpdated']=$this->counters['rowsUpToDate']=$this->counters['rowsInserted']=$this->counters['errorParsing']=$this->counters['rowsParsed']=0;
		$this->	//Удалить файл на выходе, чтобы не хранить мусор. Или не сохранять
			fileToUpload->
			saveAs('uploads/' . $this->fileToUpload->baseName . '.' . $this->fileToUpload->extension);
		$tableName=explode('-',$this->fileToUpload->baseName);
		$this->table=$tableName[0];
		$this->filename=$this->fileToUpload->baseName . '.' . $this->fileToUpload->extension;
		switch ($this->table){									//Создаем объект для записи
			case 'products':
				$activeTable= new Products;
				break;
			case 'units':
				$activeTable= new Units;
				break;
			case 'manufacturers':
				$activeTable= new Manufacturers;
				break;
			case 'brands':
				$activeTable= new Brands;
				break;
			case 'categories':
				$activeTable= new Categories;
				break;
			default:
				$this->errors[0]=['Не найдена таблица'=>[ $this->table]];
				return;
		}
		$className=$activeTable->className();

		if (!file_exists('uploads/'.$this->filename)) {			//Проверка на наличие файла
			$this->errors[0]=['Нет файла'=>[$this->filename]];
			return;
		}
		$this->fileObject = new SplFileObject('uploads/'.$this->filename);
		$this->fileObject->setFlags(SplFileObject::READ_CSV | SplFileObject::SKIP_EMPTY | SplFileObject::DROP_NEW_LINE | SplFileObject::READ_AHEAD);
		$this->fileObject->setCsvControl('|');
		$fields=$this->fileObject->current();
		if ($this->table.'Slug'!=$fields[0]){					//Проверка на ключевое поле
			$this->errors[1]=['Первое поле не ключ таблицы'=>[$fields[0]]];
			return;
		}
		foreach ($fields as $field){							//Проверка на лишние поля в таблице и создание дополнительных валидаторов для Products
			$this->columnCount++;
			if (!$activeTable->hasAttribute($field)){
				$this->errors[1]=['Лишнее поле'=>[$field]];
				return false;
			}
			if ($this->table=='products'){						//Валидаторы
				if ($field=='productsCat0'){
					$categories=ArrayHelper::getColumn(Categories::find()->all(), 'categoriesSlug');
					$validators[]= [
						'message'=>'Категория не найдена',
						'attribute'=>'productsCat0',
						'range'=>$categories,
					];
				}
				if ($field=='productsCat1'){
					if(!isset($categories)) $categories=ArrayHelper::getColumn(Categories::find()->all(), 'categoriesSlug');
					$validators[]= [
						'message'=>'Категория не найдена',
						'attribute'=>'productsCat1',
						'range'=>$categories,
					];
				}
				if ($field=='productsCat2'){
					if(!isset($categories)) $categories=ArrayHelper::getColumn(Categories::find()->all(), 'categoriesSlug');
					$validators[]= [
						'message'=>'Категория не найдена',
						'attribute'=>'productsCat2',
						'range'=>$categories,
					];
				}
				if ($field=='productsCat3'){
					if(!isset($categories)) $categories=ArrayHelper::getColumn(Categories::find()->all(), 'categoriesSlug');
					$validators[]= [
						'message'=>'Категория не найдена',
						'attribute'=>'productsCat3',
						'range'=>$categories,
					];
				}
				if ($field=='productsType'){
					if(!isset($categories)) $categories=ArrayHelper::getColumn(Categories::find()->all(), 'categoriesSlug');
					$validators[]= [
						'message'=>'Вид товара не найден',
						'attribute'=>'productsType',
						'range'=>$categories,
					];
				}
				if ($field=='productsUnitSlug'){
					$units=ArrayHelper::getColumn(Units::find()->all(), 'unitsSlug');
					$validators[]= [
						'message'=>'Единица измерения не найдена',
						'attribute'=>'productsUnitSlug',
						'range'=>$units,
					];
				}
				if ($field=='productsBrandSlug'){
					$brands=ArrayHelper::getColumn(Brands::find()->all(), 'brandsSlug');
					$validators[]= [
						'message'=>'Брэнд не найден',
						'attribute'=>'productsBrandSlug',
						'range'=>$brands,
					];
				}
				if ($field=='productsManufacturerSlug'){
					$manufacturers=ArrayHelper::getColumn(Manufacturers::find()->all(), 'manufacturersSlug');
					$validators[]= [
						'message'=>'Производитель не найден',
						'attribute'=>'productsManufacturerSlug',
						'range'=>$manufacturers,
					];
				}
			}
		}
		$this->fileObject->next();
		while($this->fileObject->valid()){
			$row=$this->fileObject->current();
			if ($currentRecord=$activeTable->findOne($row[0])){
				$updateCounterKey='rowsUpdated';
			}
			else{
				$updateCounterKey='rowsInserted';
				$currentRecord= new $className;
			}//Если таблица Products и число валидаторов недостаточно - добавляем их
			if ($this->table=='products' && count($currentRecord->validators)==$currentRecord::VALIDATORS_COUNT){
				$currentRecord->addValidators($validators);
			}
			$currentRecord->attributes=array_combine($fields, $row);
			if(!count($currentRecord->getDirtyAttributes())) $updateCounterKey='rowsUpToDate';		//Нечего обновлять
			if (!$currentRecord->save()){
				$this->counters['errorParsing']++;
				$this->errors[$this->counters['rowsParsed']+2]=$currentRecord->getErrors();
			}
			else $this->counters[$updateCounterKey]++;
			$this->counters['rowsParsed']++;
			$this->fileObject->next();
		}
		unset ($this->fileObject);
		unlink('uploads/'.$this->filename);
	}

	public function rules()
	{
		return [
			['fileToUpload', 'required'],
			[['fileToUpload'], 'file', 'skipOnEmpty' => false, 'extensions' => 'csv'],
		];
	}

	/**
	 * @return array customized attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'fileToUpload' => 'Выберите файл',
		];
	}
}
