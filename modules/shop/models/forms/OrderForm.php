<?php

namespace app\modules\shop\models\forms;
use yii\base\Model;
use app\modules\shop\models\OrdersH;
use app\modules\shop\models\OrdersD;
use app\components\Basket;

use Yii;
class OrderForm extends Model{
	public $name;
	public $email;
	public $comment;
	public $phone;
	public $deliveryDate;
	public $address;
	public $order=false;
	public function rules()
	{
		return [
			// name, phone and address are required
			[['name', 'phone', 'address', 'deliveryDate', 'email'], 'required'],
			// email has to be a valid email address
			['email', 'email'],
			[['comment','address'], 'string'],
			['deliveryDate', 'date'],
			// Телефон как целое. Возможно есть отдельное правило для телефонов
			['phone', 'integer' , 'min' => 100000],
		];
	}
	public function __construct(){
		$this->loadCookies();
	}

	/**
	 * @return array customized attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'phone' => 'Телефон',
			'comment'=>'Комментарий',
			'name'=>'Имя',
			'email'=>'Почта для подтверждения заказа',
			'address' => 'Адрес для доставки',
			'deliveryDate' => 'Дата доставки',
		];
	}
	protected function saveCookies(){
		$cookies = Yii::$app->response->cookies;
		$val=[
			'name'=>$this->name,
			'email'=>$this->email,
			'phone'=>$this->phone,
			'address'=>$this->address,
		];
		$cookies->add(new \yii\web\Cookie(
			['name' => 'orderParams', 
			'value' => $val,
			'expire'=>time() + 86400 * 365]));
	}
	protected function loadCookies(){
		if (!Yii::$app->user->isGuest){
			if (Yii::$app->user->identity->customerPhone) $this->phone=Yii::$app->user->identity->customerPhone;
			if (Yii::$app->user->identity->email) $this->email=Yii::$app->user->identity->email;
			if (Yii::$app->user->identity->userView) $this->name=Yii::$app->user->identity->userView;
			if (Yii::$app->user->identity->defaultAdress) $this->address=Yii::$app->user->identity->defaultAdress;
		}
		$val=Yii::$app->request->cookies->getValue('orderParams', false);
		
		if ($val===false) return;
		$this->name=$val['name'];
		$this->email=$val['email'];
		$this->phone=$val['phone'];
		$this->address=$val['address'];
	}
	protected function saveOrder(){
		$basket=new Basket;
		if ($basket->products===false) return false;
		$order=new OrdersH();
		$order->date=time();
		$order->customerName=$this->name;
		$order->customerPhone=$this->phone;
		$order->deliveryAddress=$this->address;
		if ($this->email) $order->email=$this->email;
		if ($this->comment) $order->comment=$this->comment;
		if ($this->deliveryDate) $order->deliveryDate=strtotime($this->deliveryDate);
//		echo $order->deliveryDate;
		if (!Yii::$app->user->isGuest) $order->userId=Yii::$app->user->identity->id;
		$order->insert();
		foreach ($basket->products as $product){
			$row= new OrdersD;
			$row->productSlug=$product['id'];
			$row->quant=$product['quant'];
			$row->price=$product['price'];
			
			$row->link('orderH',$order);
		}
		if (!YII_DEBUG) $basket->clearBasket();
		$this->order=$order;
		return true;
	}
	public function contact($email){
		if ($this->validate()) {
			$this->saveCookies();
			if (!$this->saveOrder()) return false;
			Yii::$app->mailer->compose('@app/mail/order',				//Письмо магазину
					['order' => $this->order]
				)
				->setTo($email)
				->setFrom(Yii::$app->params['mailSender'])
				->setSubject(Yii::$app->name.' - Заказ №'.$this->order->id)
				->send();

			Yii::$app->mailer->compose('@app/mail/orderConfirm',		//Письмо покупателю
					['order' => $this->order]
				)
				->setTo($email)
				->setFrom(Yii::$app->params['mailSender'])
				->setSubject(Yii::$app->name.' - Заказ №'.$this->order->id)
				->send();

			return true;
		}
		return false;
	}
}
