<?php

namespace app\modules\shop\models;
use yii\helpers\ArrayHelper;

use Yii;

/**
 * This is the model class for table "manufacturers".
 *
 * @property string $manufacturersSlug
 * @property string $manufacturersPhoto
 * @property string $manufacturersFullName
 * @property string $manufacturersShortName
 */
class Manufacturers extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'manufacturers';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['manufacturersSlug', 'manufacturersFullName'], 'required'],
			[['manufacturersSlug', 'manufacturersPhoto', 'manufacturersFullName', 'manufacturersShortName'], 'string', 'max' => 255]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'manufacturersSlug' => 'Код',
			'manufacturersPhoto' => 'Фото',
			'manufacturersFullName' => 'Полное наименование',
			'manufacturersShortName' => 'Краткое наименование',
		];
	}
	public static function getFullList(){
		$records=self::find()->all();
		return array_combine(ArrayHelper::getColumn($records,'manufacturersSlug'),ArrayHelper::getColumn($records,'manufacturersFullName'));
	}
}
