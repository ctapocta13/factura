<?php

namespace app\modules\shop\models;
use app\modules\shop\models\query\ProductsQuery;
use app\modules\user\models\User;
use yii\validators\Validator;

use Yii;

/**
 * This is the model class for table "products".
 *
 * @property string $productsSlug
 * @property string $productsPhoto
 * @property string $productsCat0
 * @property string $productsCat1
 * @property string $productsCat2
 * @property string $productsCat3
 * @property string $productsType
 * @property string $productsName
 * @property string $productsUnitSlug
 * @property integer $productsUnitValue
 * @property integer $productsBrandId
 * @property string $productsManufacturerSlug
 * @property integer $productsStock
 * @property integer $productsPrice
 * @property string $productsDescriptionFull
 * @property string $productsPDFLink
 * @property integer $productsIsAction
 * @property integer $productsActionDiscount
 * @property integer $productsIsActive
 */
class Products extends \yii\db\ActiveRecord
{
	const PHOTO_PREVIEW='pre';
	const PHOTO_MAIN='main';
	const PHOTO_ICON='icon';
	const SQU_NO=0;
	const SQU_MAIN=1;
	const SQU_SUB=2;
	const VALIDATORS_COUNT=5;//Изменить при добавлении правил. Нужно для корректного импорта из csv
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'products';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()//При изменении количества правил поправить константу VALIDATORS_COUNT
	{
		return [
			[['productsSlug', 'productsType', 'productsUnitSlug', 'productsUnitValue', 'productsCat0', 'productsBrandSlug'], 'required'],
			[[
				'productsUnitValue',
				'productsStock',
				'productsIsAction',
				'productsIsSQU',
				'productsActionDiscount',
				'productsIsActive',
				'productsPrice',
				'productsHeight',
				'productsWidth',
				'productsLong',
				'productsSize',
				'productsDiameter',
			],
			'integer'],
			['productsIsSQU', 'default', 'value' => 0],
			[['productsDescriptionFull'], 'string'],
			[[
				'productsSlug',
				'productsSQUParentId',
				'productsSQUFilterName',
				'productsBrandSlug',
				'productsPhoto0',
				'productsPhoto1',
				'productsPhoto2',
				'productsPhoto3',
				'productsCat0',
				'productsCat1',
				'productsCat2',
				'productsCat3',
				'productsType',
				'productsName',
				'productsUnitSlug',
				'productsPDFLink',
				'productsManufacturerSlug',
				'productsColor',
				'productsMaterial',
			 ],
			 'string', 'max' => 255]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels(){
		return [
			'productsSlug' => 'Код',
			'productsPhoto0' => 'Главное фото',
			'productsPhoto1' => 'Фото2',
			'productsPhoto2' => 'Фото3',
			'productsPhoto3' => 'Фото4',
			'productsCat0' => 'Категория 1',
			'productsCat1' => 'Категория 2',
			'productsCat2' => 'Категория 3',
			'productsCat3' => 'Категория 4',
			'productsType' => 'Вид товара',
			'productsName' => 'Название товара',
			'productsUnitSlug' => 'Меряем в',
			'productsUnitValue' => 'В упаковке',
			'productsBrandSlug' => 'Бренд',
			'productsManufacturerSlug' => 'Производитель',
			'productsStock' => 'Остаток',
			'productsPrice' => 'Цена',
			'productsHeight'=> 'Высота',
			'productsWidth'=> 'Ширина',
			'productsLong'=> 'Глубина',
			'productsSize'=> 'Размер',
			'productsDiameter'=> 'Диаметр',
			'productsColor' => 'Цвет',
			'productsMaterial' => 'Материал',
			'productsDescriptionFull' => 'Описание',
			'productsIsAction' => 'Акция',
			'productsActionDiscount' => 'Скидка',
			'productsIsSQU' => 'Составной товар',
			'productsSQUFilterName' => 'Фильтр составного товара',
			'productsSQUParentId' => 'Код основного товара',
			'productsIsActive' => 'Активен',
			
		];
	}
	public function getUnit(){
		return $this->hasOne(Units::className(), ['unitsSlug'=>'productsUnitSlug']);
	}
	public function getShortName(){
		return
			$this->type->categoriesShortName.
			' '.$this->brand->brandsShortName.
			($this->productsName ? ' '.$this->productsName.' ' : '' ).
			($this->productsSize ? ', Размер '.$this->productsSize : '' ).
			', '.$this->productsUnitValue.
			' '.$this->unit->unitsShortName;
	}
	public function getSubProducts(){
		return $this->hasMany(self::className(), ['productsSQUParentId'=>'productsSlug']);
	}
	public function getParentProduct(){
		return $this->hasOne(self::className(), ['productsSlug' => 'productsSQUParentId']);
	}
	public function getFullName(){
		return
			$this->type->categoriesFullName.' '.
			$this->brand->brandsFullName.' '.
			$this->productsName;
	}
	public function getAlt(){
		return $this->type->categoriesFullName.' '.
			$this->brand->brandsFullName.' '.
			$this->productsName;
	}
	public function getPhotoLink($type=self::PHOTO_MAIN, $index=0){
		if ($index > 3) return '@web/images/products/fish.jpg';//Рыба
		return file_exists('images/products/'.$this->attributes['productsPhoto'.$index]) &&
			$this->attributes['productsPhoto'.$index]!='' ? 
			'@web/images/products/'.($this->attributes['productsPhoto'.$index])		//Нормальное фото
			: '@web/images/products/fish.jpg';//Рыба
	}/*
	public function getPhotoLink2($type=self::PHOTO_MAIN){
		return file_exists('images/products/'.$this->productsPhoto1) && $this->productsPhoto1!='' ? 
			'@web/images/products/'.($this->attributes['productsPhoto1'])		//Нормальное фото
			: '@web/images/products/fish.jpg';//Рыба
	}*/
	public function getTitle(){
		return $this->type->categoriesFullName.' '.$this->brand->brandsFullName.' '.($this->productsName ? ' '.$this->productsName :'');
	}
	public function getBrand(){
		return $this->hasOne(Brands::className(), ['brandsSlug'=>'productsBrandSlug']);
	}
	public function getType(){
		return $this->hasOne(Categories::className(), ['categoriesSlug'=>'productsType']);
	}
	public function getCat0(){//Возможно это передавать как параметр
		return $this->hasOne(Categories::className(), ['categoriesSlug'=>'productsCat0']);
	}
	public function getCat1(){
		return $this->hasOne(Categories::className(), ['categoriesSlug'=>'productsCat1']);
	}
	public function getCat2(){
		return $this->hasOne(Categories::className(), ['categoriesSlug'=>'productsCat2']);
	}
	public function getCat3(){
		return $this->hasOne(Categories::className(), ['categoriesSlug'=>'productsCat3']);
	}
	public function getManufacturer(){
		return $this->hasOne(Manufacturers::className(), ['manufacturersSlug'=>'productsManufacturerSlug']);
	}
	public function getPrice(){
		if (Yii::$app->user->isGuest || !Yii::$app->user->identity->hasAttribute('personalDiscount')) $discount=0;
		else $discount=Yii::$app->user->identity->personalDiscount;
		if ($this->productsIsAction) 
			if ($this->productsActionDiscount > $discount) $discount = $this->productsActionDiscount;
		return $this->productsPrice*(100-$discount)/100;
	}
	public static function find(){
		return Yii::createObject(ProductsQuery::className(), [get_called_class()]);
	}
	public function addValidators($validators){
		foreach ($validators as $validator) 
			$this->validators[]=Validator::createValidator(
				'in',
				$this,
				$validator['attribute'],
				['range'=>$validator['range'],'message'=>$validator['message'],]
			);
	}
}
