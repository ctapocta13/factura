<?php

use yii\db\Schema;
use yii\db\Migration;

class m160419_140640_initial extends Migration
{
	//database is created with UTF-8 as 'CREATE DATABASE IF NOT EXISTS factura CHARACTER SET utf8'
	public function up()
	{
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->createTable('products', [
			'productsSlug' => Schema::TYPE_STRING.' PRIMARY KEY',
			'productsPhoto0' => $this->string(),
			'productsPhoto1' => $this->string(),
			'productsPhoto2' => $this->string(),
			'productsPhoto3' => $this->string(),
			'productsCat0' => $this->string(),
			'productsCat1' => $this->string(),
			'productsCat2' => $this->string(),
			'productsCat3' => $this->string(),
			'productsType' => $this->string()->notNull(),
			'productsName' => $this->string(),
			'productsUnitSlug' => $this->string()->notNull(),
			'productsUnitValue' => $this->string()->notNull(),
			'productsBrandSlug' => $this->string()->notNull(),
			'productsManufacturerSlug' => $this->string(),
			'productsStock' => $this->integer()->notNull()->defaultValue(0),
			'productsPrice' => $this->integer()->notNull()->defaultValue(0),
/*			'productsPrice2' =>$this->integer()->notNull()->defaultValue(0),
			'productsPrice3' =>$this->integer()->notNull()->defaultValue(0),
			'productsPrice4' =>$this->integer()->notNull()->defaultValue(0),
			'productsPrice5' =>$this->integer()->notNull()->defaultValue(0),*/
			'productsDescriptionFull' => Schema::TYPE_TEXT,
			'productsHeight'=>$this->integer(),
			'productsWidth'=>$this->integer(),
			'productsLong'=>$this->integer(),
			'productsSize'=>$this->integer(),
			'productsDiameter'=>$this->integer(),
			'productsColor' =>  $this->string(),
			'productsMaterial' => $this->string(),
			'productsIsAction' => $this->boolean()->notNull()->defaultValue(0),
			'productsActionDiscount' => $this->integer()->notNull()->defaultValue(0),
			'productsIsSQU' => $this->integer()->notNull()->defaultValue(0),
			'productsSQUFilterName' => $this->string(),
			'productsSQUParentId' => $this->string(),
			'productsPDFLink' => $this->string(),
			'productsIsActive' =>  $this->boolean()->notNull()->defaultValue(1),
			], $tableOptions
		);
		$this->createTable('brands', [
			'brandsSlug' => Schema::TYPE_STRING.' PRIMARY KEY',
			'brandsPhoto' => $this->string(),
			'brandsFullName' => $this->string()->notNull(),
			'brandsShortName' => $this->string(),
			], $tableOptions
		);
		$this->createTable('units', [
			'unitsSlug' => Schema::TYPE_STRING.' PRIMARY KEY',
			'unitsFullName' => $this->string()->notNull(),
			'unitsShortName' => $this->string()->notNull(),
			], $tableOptions
		);
		$this->createTable('manufacturers', [
			'manufacturersSlug' => Schema::TYPE_STRING.' PRIMARY KEY',
			'manufacturersPhoto' => $this->string(),
			'manufacturersFullName' => $this->string()->notNull(),
			'manufacturersShortName' => $this->string(),
			], $tableOptions
		);
		$this->createTable('categories', [
			'categoriesSlug' => Schema::TYPE_STRING.' PRIMARY KEY',
			'categoriesFullName' => $this->string()->notNull(),
			'categoriesShortName' => $this->string()->notNull(),
			], $tableOptions
		);
		$this->createTable('ordersH', [
			'id' => $this->primaryKey(),
			'userId' => $this->integer()->notNull()->defaultValue(0),
			'date' => $this->integer(),
			'customerName' => $this->string()->notNull(),
			'customerPhone' => $this->string()->notNull(),
			'deliveryAddress' => $this->string()->notNull(),
			'email' =>  $this->string(),
			'comment' =>  $this->string(),
			'deliveryDate' => $this->integer(),
			'isPayed' => $this->boolean()->notNull()->defaultValue(0),
			'isDelivered' => $this->boolean()->notNull()->defaultValue(0),
			'isCancelled' => $this->boolean()->notNull()->defaultValue(0),
			], $tableOptions
		);
		$this->createTable('ordersD', [
			'id' => $this->primaryKey(),
			'orderId' =>$this->integer()->notNull(),
			'productSlug' => $this->string(),
			'quant' => $this->integer()->notNull(),
			'price' => $this->integer()->notNull(),
			], $tableOptions
		);
		$this->createTable('user', [
			'id' => $this->primaryKey(),
			'created_at' => $this->integer()->notNull(),
			'updated_at' => $this->integer()->notNull(),
			'personalDiscount' => $this->integer()->notNull()->defaultValue(0),	//Персональная скидка в %
			'username' => $this->string()->notNull(),
			'userView' => $this->string()->notNull()->defaultValue('Новый пользователь'),
			'customerPhone' => $this->string(),
			'defaultAdress' => $this->string(),
			'auth_key' => $this->string(32),
			'email_confirm_token' => $this->string(),
			'password_hash' => $this->string()->notNull(),
			'password_reset_token' => $this->string(),
			'role'=> $this->string(64)->notNull()->defaultValue('user'),
			'email' => $this->string()->notNull(),
			'status' => $this->smallInteger()->notNull()->defaultValue(0),
		], $tableOptions);

		$this->createIndex('idx-user-username', 'user', 'username');
		$this->createIndex('idx-user-email', 'user', 'email');
		$this->createIndex('idx-user-status', 'user', 'status');
		
		$this->createIndex('idx-products-productsSQUParentId', 'products', 'productsSQUParentId');
		$this->createIndex('idx-products-productsIsSQU', 'products', 'productsIsSQU');
		
		$this->createTable('slider', [
			'id' => $this->primaryKey(),
			'sliderId' => $this->integer()->notNull(),
			'photoPath' => $this->string()->notNull(),
			'link' => $this->string()->notNull()->defaultValue('#'),
			'status' => $this->smallInteger()->notNull()->defaultValue(1),
			'sortOrder' => $this->smallInteger()->notNull()->defaultValue(0),
			'startActivity' => $this->integer()->notNull(),
			'stopActivity' => $this->integer()->notNull(),
			], $tableOptions
		);
		$this->createTable('sliderName', [
			'id' => $this->primaryKey(),
			'name' => $this->string()->notNull(),
			], $tableOptions
		);
		$this->createTable('pages', [
			'id' => $this->primaryKey(),
			'name' => $this->string()->notNull(),
			'body' =>$this->text()->notNull()->defaultValue(''),
			], $tableOptions
		);
		$this->insert('sliderName', ['name' => 'Главный слайдер']);
		
		$this->insert('pages', ['name' => 'Главная', 'body' => '<p>Введите текст...</p>']);
		$this->insert('pages', ['name' => 'Сотрудничество', 'body' => '<p>Введите текст...</p>']);
		$this->insert('pages', ['name' => 'Контакты', 'body' => '<p>Введите текст...</p>']);
		$this->insert('pages', ['name' => 'Доставка', 'body' => '<p>Введите текст...</p>']);
		$this->insert('pages', ['name' => 'Оплата', 'body' => '<p>Введите текст...</p>']);

	}

	public function down()
	{
		echo "Tables dropped.\n";
		$this->dropTable('products');
		$this->dropTable('brands');
		$this->dropTable('units');
		$this->dropTable('manufacturers');
		$this->dropTable('categories');
		$this->dropTable('ordersH');
		$this->dropTable('ordersD');
		$this->dropTable('user');
		$this->dropTable('slider');
		$this->dropTable('sliderName');
		$this->dropTable('pages');

		return true;
	}
}
