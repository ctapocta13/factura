<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;

use yii\web\Controller;

use app\models\LoginForm;
use app\models\ContactForm;
//use app\modules\shop\models\Slider;

class SiteController extends Controller
{
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['logout'],
				'rules' => [
					[
						/*'actions' => ['logout'],*/
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
		];
	}
	public function actions()
	{
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class' => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
			],
		];
	}
	public function actionIndex()
	{
	//	$model= Slider::find()->where(['status'=>1])->all();
		return $this->render('index'
//			['slider' =>$model]
		);
	}
	public function actionPartnership()
	{
		return $this->render('partnership');
	}
	public function actionPayments()
	{
		return $this->render('payments');
	}
	public function actionContact()
	{
		$model = new ContactForm();
		if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
			Yii::$app->session->setFlash('contactFormSubmitted');
			return $this->refresh();
		}
		return $this->render('contact', [
			'model' => $model,
		]);
	}
	/*
	public function actionAbout()
	{
		return $this->render('about');
	}*/
}
