$(document).ready(function () {

	$('.intoBasket').click(function(event){
		var url = 'default/add-basket';
		var clickedbtn = $(this);
		var productcSlug = clickedbtn.data("productid");
		$.ajax({
			url: url,
			type: "GET",
			data: {'id':productcSlug},
			success: function (data) {
				if (data==1) $('.in-basket').removeClass('hide');
				$('.in-basket-count').html(data);
				updateBasket();
			}
		});
	});
	function updateBasket(){
		var url = 'default/update-basket';
		$.ajax({
			url: url,
			type: "GET",
			success: function (data) {
				var basketCount=$('.basket-count');
				var result=jQuery.parseJSON(data);
				basketCount.html(result['count']);
				$('.basket-sum').html(result['sum']);
				basketCount.animate({height: 'toggle'}, 100);
				setTimeout(function() {
						$('.basket-count').animate({height: 'toggle'});
					}, 100);
			}
		});
	}
	$("#pjax").on("pjax:end", function(){
		updateBasket();
	});
	$('.product-squ-filter-item').click( function(event){
		$('.product-squ-filter-item').each(function (i, el){$(this).removeClass('product-squ-filter-item-active')});
		$(this).addClass('product-squ-filter-item-active');
		var basketButton=$('.intoBasket');
		basketButton.removeClass('disabled');
		basketButton.data('productid', $(this).data('productid'));
		$('.products-details-price-value').html($(this).data('price'));
	});
	$('.photo-button').click( function(event){
		$('.product-image-wrapper-full').each(function (i, el){$(this).addClass('hidden')});
		$('#'+$(this).data('photoid')).removeClass('hidden');
	});
});
