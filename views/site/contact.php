<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\components\widgets\pages\PageWidget;

$this->title = 'Контакты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">
	<h2><?= Html::encode($this->title) ?></h2>

	<?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

		<div class="alert alert-success">
			Спасибо, что связались с нами. Мы ответим как только сможем
		</div>

		<p>
			<?php if (Yii::$app->mailer->useFileTransport): ?>
				Because the application is in development mode, the email is not sent but saved as
				a file under <code><?= Yii::getAlias(Yii::$app->mailer->fileTransportPath) ?></code>.
				Please configure the <code>useFileTransport</code> property of the <code>mail</code>
				application component to be false to enable email sending.
			<?php endif; ?>
		</p>

	<?php else: ?>
		<div class="row">
			<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
				<?=PageWidget::widget(['id' => 3]);?>
			</div>
		</div>

		<p>
			С удовольствием ответим на ваши вопросы
		</p>

		<div class="row">
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">

				<?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>

					<?= $form->field($model, 'name') ?>

					<?= $form->field($model, 'email') ?>

					<?= $form->field($model, 'subject') ?>

					<?= $form->field($model, 'body')->textArea(['rows' => 6]) ?>

					<?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
						'template' => '<div class="row"><div class="col-lg-4">{image}</div><div class="col-lg-6">{input}</div></div>',
					]) ?>

					<div class="form-group">
						<?= Html::submitButton('Отправить', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
					</div>

				<?php ActiveForm::end(); ?>

			</div>
		</div>

	<?php endif; ?>
</div>
