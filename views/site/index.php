<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
use app\components\widgets\pages\PageWidget;
use app\components\widgets\slider\SliderWidget;
$this->title = 'Главная';
?>
<div class="site-index">
	<p class="lead"></p>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
			<?=SliderWidget::widget();?>
		</div>
		<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
			<?=PageWidget::widget(['id' => 1]);?>
		</div>
	</div>
</div>
