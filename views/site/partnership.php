<?php
use app\components\widgets\pages\PageWidget;
/* @var $this yii\web\View */

$this->title = 'Сотрудничество';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-partnership">
	<h2><?=$this->title?></h2>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
			<?=PageWidget::widget(['id' => 3]);?>
		</div>
	</div>
