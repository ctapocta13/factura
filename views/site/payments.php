<?php
//use yii\helpers\Html;
/* @var $this yii\web\View */
use app\components\widgets\pages\PageWidget;

$this->title = 'Оплата';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-payments">
	<h2><?=$this->title?></h2>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
			<?=PageWidget::widget(['id' => 5]);?>
		</div>
	</div>
