<?php

/* @var $this \yii\web\View */
/* @var $content string */
use app\assets\AppAsset;
use app\components\widgets\BasketWidget;
use app\components\widgets\DiscountWidget;
use app\components\widgets\Alert;

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\bootstrap\BootstrapAsset;

use yii\widgets\Breadcrumbs;

//use app\widgets\SideMenuWidget;
use app\rbac\Rbac as AdminRbac;
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	<!--<script> Счетчик гугла
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-56897422-4', 'auto');
		ga('send', 'pageview');
	</script>-->
	<?= Html::csrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
	<?php
	
	NavBar::begin([
		'brandLabel' => '<div class="logo-image">'.Html::img('@web/images/logo.png',[
					'class'=>'img-responsive',
					'alt'=>'Домашний декор',
				]).'</div>',//'Главная',
		'brandUrl' => Yii::$app->homeUrl,
		'options' => [
			'class' => 'navbar-static-top navbar-fixed-top navbar-default',//navbar-inverse 
		],
	]);
	$items[]=['label' => 'Отделочные материалы', 'url' => ['/shop/catalog', 'id'=>'']];
	$items[]=['label' => 'Текстиль', 'url' => ['/shop/catalog', 'id'=>'']];
	$items[]=['label' => 'Сотрудничество', 'url' => ['/site/partnership']];
	$items[]=['label' => 'Оплата', 'url' => ['/site/payments']];
	$items[]=['label' => 'Контакты', 'url' => ['/site/contact']];

	if (Yii::$app->user->isGuest)
		$items[]=['label' => 'Войти', 'url' => ['/user/default/login'],];
	else {
		if (\Yii::$app->user->can(AdminRbac::PERMISSION_CONTENT_EDIT)) $loginItems[]=
			[
				'label' => 'Управление магазином',
				'url' => ['/shop/admin'],
			];
		$loginItems[]=
				[
					'label' => 'Личный кабинет',
					'url' => ['/user/profile/index'],
				];
		$loginItems[]=
				[
					'label' => 'Выход',
					'url' => ['/user/default/logout'],
					'linkOptions' => ['data-method' => 'post']
				];
		$items[]=[
			'label' => Yii::$app->user->identity->userView,
			'items' => $loginItems,
		];
	}

	echo Nav::widget([
		'options' => ['class' => 'navbar-nav navbar-left'],
		'items' => $items
	]);
	NavBar::end();
	?>
	<div class="basket-box"><?= BasketWidget::widget()?></div>
	<div class="container-fluid  main-content">
		<div class="row">
			<?= Breadcrumbs::widget(['links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],]) ?>
			<?= Alert::widget() ?>
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<?= $content ?>
			</div>
		</div>
	</div>
</div>

<footer class="footer">
	<div class="container-fluid">
		<p class="pull-left text-justify">Все ЦЕНЫ и МАТЕРИАЛЫ, указанные на сайте, приведены как справочная информация и не являются публичной офертой, определяемой положениями ст. 437 ГК РФ, и могут быть изменены в любое время без предупреждения. Для получения подробной информации о стоимости, сроках и условиях поставки просьба обращаться по указанным на сайте телефонам.</p>
		<p class="pull-right">&copy; <?= Yii::$app->name?> <?= date('Y') ?><?= ',  '.Html::mailto('Ушаков А.', 'ctapocta13@gmail.com')?></p>
		<p class="pull-left"><?php //= Yii::powered() ?></p>

	</div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
